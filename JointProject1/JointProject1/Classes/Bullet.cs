﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description:
 * This is the Bullet class which,
 * represents the bullet sprite,
 * used by the Player Class and EnemyRanged Class
 */

namespace JointProject1
{
    class Bullet
    {
        #region VARIABLES

        // Will store and alter the position of the bullet
        Vector2 position;

        // Will track the objects (the one firing this bullet) position
        Vector2 shotOrigin;

        // Will store the direction and velocity the bullet is moving
        Vector2 velocity;

        // Will be used to store the limitations of the bullet,
        // to be checked against the other sprites for collision
        Rectangle boundaryBox;

        // Will be used to store the center of the rotation of the bullet
        Vector2 rotationPosition;

        // Will be used to store the center location of the bullet texture,
        // to be used in the Draw() method
        Vector2 centerDrawPosition;
        
        // Currect Texture
        Texture2D bulletTexture;

        // To preload texture for different bullet states
        Texture2D bulletAliveTexture;
        Texture2D bulletDeadTexture;

        // Represents the rotation/direction in degrees the bullet is facing
        float rotationAngle;

        // Used by the game class to determine whether the bullet,
        // is alive or dead
        bool alive;

        // Will track in what state the bullet is in
        enum BulletState
        {
            Alive, Dead
        }
        BulletState bulletState;

        // Speed of the bullet
        int speed = 10;

        #endregion

        #region CONSTANTS

        // Dimensions of the bullet
        const int width = 20;
        const int height = 60;
        
        // bullet texture file
        const string TextureFile = "_bullet_alive";

        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Default Constructor
        /// </summary>
        public Bullet()
        {
            int x = -300;
            int y = -300;

            position = new Vector2(x, y);
            boundaryBox = new Rectangle(x, y, width, height);
            centerDrawPosition.X = boundaryBox.Center.X;
            centerDrawPosition.Y = boundaryBox.Center.Y;
            velocity = new Vector2();

            bulletState = BulletState.Dead;
            rotationAngle = 0f;

        } // End of Bullet()
        /// <summary>
        /// Bullet Constructor,
        /// at a particular location
        /// </summary>
        /// <param name="location">vector where the bullet is constructed</param>
        public Bullet(Vector2 location)
        {
            //Vector2 toCenterPosition = new Vector2(width / 2, height / 2);
            //Vector2 toTopLeftPosition = new Vector2(-(width / 2), -(height));
            //Vector2 toFiringPosition = new Vector2(0, -Global.PlayerHeight);
            //position = location + toFiringPosition + toTopLeftPosition;
            position = location;
            shotOrigin = location;
            boundaryBox = new Rectangle((int)position.X, (int)position.Y, width, height);
            //centerDrawPosition = position + toCenterPosition;
            velocity = new Vector2();

            bulletState = BulletState.Dead;
            rotationAngle = 0f;

        } // End of Bullet(location)
        /// <summary>
        /// Bullet Constructor,
        /// at a particular location,
        /// at a certain speed
        /// </summary>
        /// <param name="location">vector where the bullet is constructed</param>
        /// <param name="choosenSpeed"></param>
        public Bullet(Vector2 location, int choosenSpeed)
        {
            position = location;
            shotOrigin = location;
            boundaryBox = new Rectangle((int)position.X, (int)position.Y, width, height);
            //centerDrawPosition = position + toCenterPosition;
            velocity = new Vector2();

            speed = choosenSpeed;
            bulletState = BulletState.Dead;
            rotationAngle = 0f;

        } // End of Bullet(Vector2,int)

        #endregion

        #region PUBLIC METHODS

        /// <summary>
        /// Will handle loading in external content (Audio and Textures)
        /// </summary>
        /// <param name="assetManager"></param>
        /// <param name="assetName"></param>
        public void LoadContent(ContentManager assetManager, string assetName)
        {
            bulletAliveTexture = assetManager.Load<Texture2D>(assetName + TextureFile);
            //bulletDeadTexture = assetManager.Load<Texture2D>(assetName + "_bullet_alive");

            bulletTexture = bulletAliveTexture;
            centerDrawPosition = new Vector2((float)(bulletTexture.Width / 2), (float)(bulletTexture.Height / 2));

        } // End of LoadContent()
        /// <summary>
        /// Will update the bullets spawning position
        /// </summary>
        /// <param name="firingPosition"></param>
        public void Update(Vector2 spawnLocation)
        {
            switch (bulletState)
            {
                case BulletState.Alive:

                    Move();

                    break;
                case BulletState.Dead:

                    SpawnPoint(spawnLocation);

                    break;
                default:
                    // nothing
                    break;
            }
            AliveTracker();
            LocationUpdate();

        } // End of Update()
        /// <summary>
        /// Will draw the object
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            TextureUpdate();
            if (alive)
            {
                spriteBatch.Draw(bulletTexture, boundaryBox, null, Color.White, AngleToRadians(rotationAngle),
                    centerDrawPosition, SpriteEffects.None, 0.0f);
            }
            else
            {
                // Bullet is dead, so it will not draw
            }

        } // End of Draw()
        /// <summary>
        /// Fires in the angle passed as a argument,
        /// by setting it to alive
        /// </summary>
        /// <param name="directionVelocity"></param>
        /// <param name="rotation"></param>
        public void Fire(float rotation)
        {
            bulletState = BulletState.Alive;
            rotationAngle = rotation;

        } // End of Fire()
        /// <summary>
        /// Will make the bullet set to the dead state
        /// </summary>
        public void Die()
        {
            bulletState = BulletState.Dead;
            position = shotOrigin;
            velocity = new Vector2(0);

        } // End of Die()
        /// <summary>
        /// Will handle bullet border collisions
        /// </summary>
        public void BorderCollisions()
        {
            switch (bulletState)
            {
                case BulletState.Alive:

                    CollisionWithScreenBorder();
                    
                    break;
                case BulletState.Dead:
                    // do nothing
                    break;
                default:
                    // do nothing
                    break;
            }

        } // End of ComputeCollisions()

        #endregion

        #region CLASS PROPERTIES

        /// <summary>
        /// will return the boundary box for collisions
        /// </summary>
        public Rectangle BoundaryBox
        {
            get
            {
                return boundaryBox;
            }

        } // End of BoundaryBox
        /// <summary>
        /// returns whether bullet is alive or not
        /// </summary>
        public bool Alive
        {
            get
            {
                return alive;
            }
        } // End of Alive
        /// <summary>
        /// Gets or sets the rotation of the bullet
        /// </summary>
        public float RotationAngle
        {
            get
            {
                return rotationAngle;
            }
            set
            {
                rotationAngle = value;
            }
        } // End of RotationAngle
        /// <summary>
        /// Gets the center position that the bullet rotates on
        /// </summary>
        public Vector2 CenterPosition
        {
            get
            {
                return centerDrawPosition;
            }
        } // End of CenterPosition
        /// <summary>
        /// Gets the bullets width
        /// </summary>
        public int Width
        {
            get
            {
                return width;
            }

        } // End of Width
        /// <summary>
        /// Gets the height of the bullet
        /// </summary>
        public int Height
        {
            get
            {
                return height;
            }

        } // End of Height
        /// <summary>
        /// Gets the position vector of the bullet
        /// </summary>
        public Vector2 Position
        {
            get
            {
                return position;
            }

        } // End of Position

        #endregion

        #region PRIVATE METHODS

        /// <summary>
        /// Will handle collisions,
        /// specifically with the screen border
        /// </summary>
        private void CollisionWithScreenBorder()
        {
            Rectangle border = new Rectangle(
                Global.screenLeftSide,
                Global.screenTopSide,
                Global.screenRightSide,
                Global.screenBottomSide);

            if (position.Y < border.Top - boundaryBox.Height)
            {
                Die();
            }
            else if (position.Y > border.Bottom + boundaryBox.Height)
            {
                Die();
            }
            else if (position.X < border.Left - boundaryBox.Width)
            {
                Die();
            }
            else if (position.X > border.Right + boundaryBox.Width)
            {
                Die();
            }

        } // End of CollisionWithScreenBorder()
        /// <summary>
        /// Will alter the update based on the state of the player
        /// alive or dead
        /// </summary>
        private void TextureUpdate()
        {
            if (alive == true)
            {
                bulletTexture = bulletAliveTexture;
            }
            else if (alive == false) // Player is dead
            {
                bulletTexture = bulletDeadTexture;
            }

        } // End of TextureUpdate()
        /// <summary>
        /// Will update all location variables based off the position vector
        /// </summary>
        private void LocationUpdate()
        {
            boundaryBox.X = (int)position.X;
            boundaryBox.Y = (int)position.Y;

        } // End of LocationUpdate()
        /// <summary>
        /// Updates alive bool for,
        /// use in class property
        /// </summary>
        private void AliveTracker()
        {
            if (bulletState != BulletState.Dead)
            {
                alive = true;
            }
            else
            {
                alive = false;
            }

        } // End of AliveTracker()
        /// <summary>
        /// Will update the spawn area of the bullet
        /// </summary>
        private void SpawnPoint(Vector2 location)
        {
            position = location;
            boundaryBox = new Rectangle((int)position.X, (int)position.Y, width, height);
            velocity = new Vector2();

        } // End of SpawnPoint()
        /// <summary>
        /// Will handle bullet movement based on the direction its facing
        /// </summary>
        private void Move()
        {
            MoveForwards();
            position += velocity;

        } // End of Move()
        /// <summary>
        /// Will move forwards, the bullet based on the direction its faced
        /// by applying the rotation angle to its velocity vector
        /// </summary>
        private void MoveForwards()
        {
            velocity = new Vector2(0, speed);
            velocity = RotateVector(velocity, rotationAngle);

        } // End of MoveForwards()
        /// <summary>
        /// The vector to rotate will be rotated,
        /// by angle in degrees,
        /// around it own vector,
        /// by using the following formula:
        /// X =
        /// -(cos(radians) * velocity.X - sin(radians) * velocity.Y)
        /// Y = 
        /// (sin(radians) * velocity.X - cos(radians) * velocity.Y)
        /// </summary>
        /// <param name="vectorToRotate"></param>
        /// <param name="angleInDegrees"></param>
        /// <returns></returns>
        private Vector2 RotateVector(Vector2 vectorToRotate, float angleInDegrees)
        {
            double angleInRadians = AngleToRadians(angleInDegrees);
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);

            return new Vector2(
                (float)
                -(cosTheta * vectorToRotate.X - sinTheta * vectorToRotate.Y),
                (float)
                (sinTheta * vectorToRotate.X - cosTheta * vectorToRotate.Y)
                );

        } // End of RotateVector()
        /// <summary>
        /// Will convert degrees into radians
        /// </summary>
        /// <param name="inDegrees"> angle in degrees</param>
        private float AngleToRadians(float inDegrees)
        {
            return (float)(inDegrees * (Math.PI / 180));

        } // End of AngleToRadians()

        #endregion

    } // End of class

} // End of namespace
