﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description:
 * Enemy melee class will represent
 * the enemy that will attempt to
 * ram the player
 */

namespace JointProject1
{
    class EnemyMelee
    {
        #region VARIABLES

        // Will store and alter the position of the enemy
        Vector2 position;

        // Will store the direction and velocity the enemy is moving
        Vector2 velocity;

        // Will be used to store the limitations of the enemy,
        // to be checked against the other sprites for collision
        Rectangle boundaryBox;

        // Will be used to store the center location of the enemy,
        // to be used in the Draw() method
        Vector2 centerPosition;

        // Will be used to play the enemies explosion animation,
        // as a source rectangle array to be assigned to explosionFrame
        Rectangle[] explosionSource;

        // Will be used as the source rectangle to draw,
        // enemies explosion animation
        Rectangle explosionFrame;

        // Small integer to itterate through,
        // the explosion source rectangle array
        Int16 explosionAnimFrame;

        // To animate explosion
        Texture2D textureExplosionAnim;

        // Current Texture
        // This will be the texture that is drawn
        Texture2D textureDraw;

        // Texture variable for alive or dead
        Texture2D textureAlive;
        Texture2D textureDead;

        // Texture variable for each direction
        Texture2D textureAliveUp;
        Texture2D textureAliveRight;
        Texture2D textureAliveDown;
        Texture2D textureAliveLeft;

        // Will be used to store enemy sounds
        // enemy dying
        SoundEffect soundDie;

        // Used to detect the state the enemy is in
        enum EnemyState
        {
            Alive, Dead, NoPlayer, Dying
        }
        EnemyState enemyState;

        // Used to detect the enemies direction
        enum EnemyDirection
        {
            Up, Right, Down, Left
        }
        EnemyDirection enemyDirection;

        // used to count up until respawn time is hit
        // to respawn the enemy
        int respawnTimer;

        // used to see if enemy is alive
        bool alive;

        #endregion

        #region CONSTANTS

        // enemy dimensions
        const int Width = 40;
        const int Height = 40;

        // enemy spawn location
        const int SpawnLocX = 1000;
        const int SpawnLocY = 300;

        // enemy movement speed
        const int MovementSpeed = 1;

        // how long it takes a enemy to respawn (per game tick)
        const int RespawnTime = 200;

        // where enemy will be while respawning
        const int RespawnLocX = -300;
        const int RespawnLocY = -300;

        // Will represent max player explosion animation
        const int MaxExplosionFrames = 38;

        // Will represent the dimensions of the player explosion
        const int ExplosionWidth = 100;
        const int ExplosionHeight = 100;

        // enemy explosion animation spritesheet
        const string ExplosionAnimationFile = "explosionsheet";
        // enemy explosion animation folder
        const string ExplosionAnimationFolder = "animation/explosion/";

        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Default Constructor
        /// </summary>
        public EnemyMelee()
        {
            int x = SpawnLocX;
            int y = SpawnLocY;
            respawnTimer = 0;

            position = new Vector2(x, y);
            boundaryBox = new Rectangle(x, y, Width, Height);
            centerPosition = new Vector2((boundaryBox.Right - boundaryBox.Left) / 2, (boundaryBox.Bottom - boundaryBox.Top) / 2);

            // constructing explosion animation
            explosionSource = new Rectangle[MaxExplosionFrames];
            InitializingDyingAnimationSourceRectangles();
            explosionAnimFrame = MaxExplosionFrames - 1;

            enemyState = EnemyState.Alive;
            enemyDirection = EnemyDirection.Up;
            alive = false;

        } // End of EnemyMelee()

        /// <summary>
        /// Enemy Constructor,
        /// constructed at random locations,
        /// x value between 600 to 900,
        /// y value between 150 to 550
        /// </summary>
        /// <param name="Rng">Random Number Generator</param>
        public EnemyMelee(Random Rng)
        {
            int x = SpawnLocX;
            int y = Rng.Next(50, 550);
            respawnTimer = 0;

            position = new Vector2(x, y);
            boundaryBox = new Rectangle(x, y, Width, Height);
            centerPosition = new Vector2(
                (boundaryBox.Right - boundaryBox.Left) / 2,
                (boundaryBox.Bottom - boundaryBox.Top) / 2);

            // constructing explosion animation
            explosionSource = new Rectangle[MaxExplosionFrames];
            InitializingDyingAnimationSourceRectangles();
            explosionAnimFrame = MaxExplosionFrames - 1;

            enemyState = EnemyState.Alive;
            enemyDirection = EnemyDirection.Up;
            alive = false;

        } // End of EnemyMelee()

        #endregion

        #region PUBLIC METHODS

        /// <summary>
        /// Will load all external content
        /// </summary>
        /// <param name="assetManager"></param>
        /// <param name="assetName"></param>
        public void LoadContent(ContentManager assetManager, string assetName)
        {
            /* Loading external textures content */
            // basic alive texture
            textureAlive = assetManager.Load<Texture2D>(assetName + "enemyship_alive");
            // up facing alive texture
            textureAliveUp = assetManager.Load<Texture2D>(assetName + "enemyship_alive_up");
            // down facing alive texture
            textureAliveDown = assetManager.Load<Texture2D>(assetName + "enemyship_alive_down");
            // left facing alive texture
            textureAliveLeft = assetManager.Load<Texture2D>(assetName + "enemyship_alive_left");
            // right facing alive texture
            textureAliveRight = assetManager.Load<Texture2D>(assetName + "enemyship_alive_right");
            // sets current texture to alive
            textureDraw = textureAlive;
            // basic dead texture
            textureDead = assetManager.Load<Texture2D>(assetName + "enemyship_dead");
            // dying animation texture sheet
            LoadDyingAnimation(assetManager, assetName + ExplosionAnimationFolder);

            /* Loading external sound content */
            // dying sound
            soundDie = assetManager.Load<SoundEffect>(assetName + "enemyship_sound_dead");

        } // End of LoadContent()
        /// <summary>
        /// Allows the enemy to run logic such as updating itself,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="player">Used for the enemy to track the player</param>
        public void Update(Player player)
        {
            FollowPlayer(player);
            AliveTracker();

            switch (enemyState)
            {
                case EnemyState.NoPlayer:
                    MoveNowhere();
                    break;
                case EnemyState.Alive:
                    // when enemy is alive do stuff
                    Move();
                    break;
                case EnemyState.Dying:
                    // run enemy dying stuff
                    RunDyingAnimation();

                    break;
                case EnemyState.Dead:
                    // when enemy is dead start respawning him
                    ReSpawning();
                    
                    break;
                default:
                    // nothing
                    break;
            } // End of switch()

            TextureUpdater();

        } // End of Update()
        /// <summary>
        /// enemy will draw itself,
        /// using the boundary box,
        /// as a container
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            switch (enemyState)
            {
                case EnemyState.Alive:

                    spriteBatch.Draw(textureDraw, boundaryBox, Color.White);

                    break;
                case EnemyState.Dying:

                    // draws explosion at enemy location
                    spriteBatch.Draw(textureExplosionAnim, boundaryBox, explosionFrame, Color.White);

                    break;
                case EnemyState.Dead:

                    // when enemy is dead stop drawing him

                    break;
                case EnemyState.NoPlayer:

                    spriteBatch.Draw(textureDraw, boundaryBox, Color.White);

                    break;
                default:
                    // nothing
                    break;
            } // End of switch()

        } // End of Draw()
        /// <summary>
        /// For the game class to call,
        /// when the enemy has collided with the player
        /// </summary>
        public void HitPlayer()
        {
            Die();

        } // End of HitPlayer()
        /// <summary>
        /// For the game class,
        /// when the enemy gets hit by a bullet
        /// </summary>
        public void BulletHit()
        {
            Die();

        } // End of BulletHit()

        #endregion

        #region CLASS PROPERTIES

        /// <summary>
        /// Gets the boundary box
        /// </summary>
        public Rectangle BoundaryBox
        {
            get
            {
                return boundaryBox;
            }

        } // End of BoundaryBox
        /// <summary>
        /// Gets the alive bool,
        /// to see if enemy is alive
        /// </summary>
        public bool Alive
        {
            get { return alive; }
        }

        #endregion

        #region PRIVATE METHODS

        /// <summary>
        /// Will kill itself,
        /// set its state to dead,
        /// to start respawning
        /// </summary>
        private void Die()
        {
            soundDie.Play();
            enemyState = EnemyState.Dying;

        } // End of Die()
        /// <summary>
        /// Will run every update,
        /// while enemy is dead,
        /// with a timer counting up until RespawnTime is hit,
        /// also place enemy at RespawnLocX, RespawnLocY while respawning
        /// </summary>
        private void ReSpawning()
        {
            Place(RespawnLocX, RespawnLocY);
            if (respawnTimer >= RespawnTime)
            {
                ReSpawn();
            }
            else
            {
                respawnTimer++;
            }

        } // End of ReSpawning()
        /// <summary>
        /// Will place the enemy at the specified location
        /// </summary>
        /// <param name="x">x coordinate</param>
        /// <param name="y">y coordinate</param>
        private void Place(int x, int y)
        {
            position = new Vector2(x, y);
            LocationUpdate();

        } // End of Place()
        /// <summary>
        /// Will ReSpawn the enemy,
        /// at a random location
        /// </summary>
        private void ReSpawn()
        {
            int x = SpawnLocX;
            int y = Global.randNumGen.Next(150, 550);
            respawnTimer = 0;

            position = new Vector2(x, y);
            boundaryBox = new Rectangle(x, y, Width, Height);
            centerPosition = new Vector2((boundaryBox.Right - boundaryBox.Left) / 2, (boundaryBox.Bottom - boundaryBox.Top) / 2);

            enemyState = EnemyState.Alive;

        } // End of ReSpawn()
        /// <summary>
        /// Will update the current texture,
        /// by what direction the enemy is in
        /// </summary>
        private void TextureUpdater()
        {
            switch (enemyState)
            {
                case EnemyState.NoPlayer:
                case EnemyState.Alive:

                    switch (enemyDirection)
                    {
                        case EnemyDirection.Up:
                            textureDraw = textureAliveUp;
                            break;
                        case EnemyDirection.Right:
                            textureDraw = textureAliveRight;
                            break;
                        case EnemyDirection.Down:
                            textureDraw = textureAliveDown;
                            break;
                        case EnemyDirection.Left:
                            textureDraw = textureAliveLeft;
                            break;
                        default:
                            break;

                    } // End of switch()

                    break;
                case EnemyState.Dead:
                    textureDraw = textureDead;
                    break;
                case EnemyState.Dying:
                default:
                    // nothing
                    break;
            }

        } // End of TextureUpdater()
        /// <summary>
        /// Will set the enemy's direction based on the passed player
        /// </summary>
        /// <param name="player">player object passed</param>
        private void FollowPlayer(Player player)
        {
            if (player.Alive == true)
            {
                if (boundaryBox.X < player.BoundaryBox.X - boundaryBox.Width)
                {
                    enemyDirection = EnemyDirection.Right;
                }
                if (boundaryBox.X > player.BoundaryBox.X + player.BoundaryBox.Width)
                {
                    enemyDirection = EnemyDirection.Left;
                }
                if (boundaryBox.Y < player.BoundaryBox.Y - boundaryBox.Height)
                {
                    enemyDirection = EnemyDirection.Down;
                }
                if (boundaryBox.Y > player.BoundaryBox.Y + player.BoundaryBox.Height)
                {
                    enemyDirection = EnemyDirection.Up;
                }
            }
            else
            {
                enemyState = EnemyState.NoPlayer;
            }
            

        } // End of FollowPlayer()
        /// <summary>
        /// Will move the enemy based on,
        /// his direction
        /// </summary>
        private void Move()
        {
            switch (enemyDirection)
            {
                case EnemyDirection.Up:
                    MoveUp();
                    break;
                case EnemyDirection.Right:
                    MoveRight();
                    break;
                case EnemyDirection.Down:
                    MoveDown();
                    break;
                case EnemyDirection.Left:
                    MoveLeft();
                    break;
                default:
                    MoveNowhere();
                    break;
            } // End of switch()
            Go(); // This will move the enemy
            LocationUpdate();

        } // End of Move()
        /// <summary>
        /// Will update the boundary box,
        /// to the enemy's position
        /// </summary>
        private void LocationUpdate()
        {
            boundaryBox.X = (int)position.X;
            boundaryBox.Y = (int)position.Y;

        } // End of LocationUpdate()
        /// <summary>
        /// Will add the velocity vector,
        /// to the position vector,
        /// changing the enemies position
        /// </summary>
        private void Go()
        {
            position += velocity;

        } // End of Go()
        /// <summary>
        /// Will set players velocity vector to up
        /// </summary>
        private void MoveUp()
        {
            velocity = new Vector2(0, -MovementSpeed);

        } // End of MoveUp()
        /// <summary>
        /// Will set players velocity vector to down
        /// </summary>
        private void MoveDown()
        {
            velocity = new Vector2(0, MovementSpeed);

        } // End of MoveDown()
        /// <summary>
        /// Will set players velocity vector to right
        /// </summary>
        private void MoveRight()
        {
            velocity = new Vector2(MovementSpeed, 0);

        } // End of MoveRight()
        /// <summary>
        /// Will set players velocity vector to left
        /// </summary>
        private void MoveLeft()
        {
            velocity = new Vector2(-MovementSpeed, 0);
            
        } // End of MoveLeft()
        /// <summary>
        /// Sets the enemy's velocity vector to 0
        /// </summary>
        private void MoveNowhere()
        {
            velocity = new Vector2(0, 0);

        } // End of MoveNowhere()
        /// <summary>
        /// Will tracks the enemy's state and,
        /// set the alive bool correspondingly
        /// </summary>
        private void AliveTracker()
        {
            switch (enemyState)
            {
                case EnemyState.NoPlayer:
                case EnemyState.Alive:
                    alive = true;
                    break;
                case EnemyState.Dying:
                case EnemyState.Dead:
                    alive = false;
                    break;
                default:
                    // nothing
                    break;
            }

        } // End of AliveTracker()
        /// <summary>
        /// Will load the dying explosion animation,
        /// using a sprite sheet
        /// </summary>
        /// <param name="assetManager"></param>
        /// <param name="assetName"></param>
        private void LoadDyingAnimation(ContentManager assetManager, string assetName)
        {
            textureExplosionAnim = assetManager.Load<Texture2D>(assetName + ExplosionAnimationFile);

        } // End of LoadDyingAnimation()
        /// <summary>
        /// Will create a array of rectangles,
        /// which will have every frame of the explosion
        /// starting at 0 up to 37 (total of 38 frames)
        /// </summary>
        private void InitializingDyingAnimationSourceRectangles()
        {
            int index = 0;
            for (int y = 600; y >= 0; y -= 100)
            {
                for (int x = 500; x >= 0; x -= 100)
                {
                    if (!(x > 100 && y == 600))
                    {
                        explosionSource[index] = new Rectangle(x, y, ExplosionWidth, ExplosionHeight);
                        index++;
                    }
                }
            }

        } // End of InitializingDyingAnimation()
        /// <summary>
        /// Will run dying animation,
        /// by itterating through,
        /// the source rectangle array
        /// than setting the enemy state to dead
        /// </summary>
        private void RunDyingAnimation()
        {
            if (explosionAnimFrame >= 0)
            {
                explosionFrame = explosionSource[explosionAnimFrame];
                explosionAnimFrame--;
            }
            else
            {
                explosionAnimFrame = MaxExplosionFrames - 1;
                enemyState = EnemyState.Dead;
            }

        } // End of RunDyingAnimation()

        #endregion

    } // End of class

} // End of namespace
