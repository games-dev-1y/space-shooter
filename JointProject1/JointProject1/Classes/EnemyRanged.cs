﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description:
 * This is the enemy ranged class
 * which represents the enemy which will attempt to
 * fire at the player
 */

namespace JointProject1
{
    class EnemyRanged
    {
        #region VARIABLES

        /* Will store and alter the position of the enemy */
        Vector2 position;

        // Will store the direction and velocity the enemy is moving
        Vector2 velocity;

        // Will be used to store the limitations of the enemy,
        // to be checked against the other sprites for collision
        Rectangle boundaryBox;

        // Will be used to store the center location of the enemy,
        // to be used in the Draw() method
        Vector2 centerPosition;

        // Will be used as the bullet for this enemy
        Bullet bullet;

        // Will be used to play the enemies explosion animation,
        // as a source rectangle array to be assigned to explosionFrame
        Rectangle[] explosionSource;

        // Will be used as the source rectangle to draw,
        // enemies explosion animation
        Rectangle explosionFrame;

        // Small integer to itterate through,
        // the explosion source rectangle array
        Int16 explosionAnimFrame;

        // To animate explosion
        Texture2D textureExplosionAnim;

        // Current Texture
        // This will be the texture that is drawn
        Texture2D textureDraw;

        // Texture variable for alive or dead
        Texture2D textureAlive;
        Texture2D textureDead;

        // Texture variable for each direction
        Texture2D textureAliveUp;
        Texture2D textureAliveRight;
        Texture2D textureAliveDown;
        Texture2D textureAliveLeft;

        // Will be used to store enemy sounds
        // enemy dying
        SoundEffect soundDie;
        // enemy firing mah lazors
        SoundEffect soundFire;

        // Used to detect the state the enemy is in
        enum EnemyState
        {
            Alive, Dead, NoPlayer, Dying
        }
        EnemyState enemyState;

        // Used to detect the enemies direction
        enum EnemyDirection
        {
            Up, Right, Down, Left
        }
        EnemyDirection enemyDirection;

        // used to count up until respawn time is hit
        // to respawn the enemy
        int respawnTimer;

        // used to known if enemy is in line of sight of the player
        bool inLineOfSight;

        // used to known if enemy is alive
        bool alive;

        #endregion

        #region CONSTANTS

        /* enemy dimensions */
        const int Width = 40;
        const int Height = 40;

        // enemy spawn location
        const int SpawnLocX = 960;
        const int SpawnLocY = 300;

        // enemy movement speed
        const int MovementSpeed = 1;

        // enemy bullet speed
        const int BulletSpeed = 3;

        // how long it takes a enemy to respawn (per game tick)
        const int RespawnTime = 400;

        // where enemy will be while respawning
        const int RespawnLocX = -300;
        const int RespawnLocY = -300;

        // for the bullets firing angle
        const float Up = 0f;
        const float Down = 180f;
        const float Right = 90f;
        const float Left = -90f;

        // Will represent max player explosion animation
        const int MaxExplosionFrames = 38;

        // Will represent the dimensions of the player explosion
        const int ExplosionWidth = 100;
        const int ExplosionHeight = 100;

        // enemy explosion animation spritesheet
        const string ExplosionAnimationFile = "explosionsheet";
        // enemy explosion animation folder
        const string ExplosionAnimationFolder = "animation/explosion/";
        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Default Constructor
        /// </summary>
        public EnemyRanged()
        {
            int x = SpawnLocX;
            int y = SpawnLocY;
            respawnTimer = 0;
            inLineOfSight = false;

            position = new Vector2(x, y);
            boundaryBox = new Rectangle(x, y, Width, Height);
            centerPosition = new Vector2(
                (boundaryBox.Right - boundaryBox.Left) / 2,
                (boundaryBox.Bottom - boundaryBox.Top) / 2
                );
            bullet = new Bullet();

            // constructing explosion animation
            explosionSource = new Rectangle[MaxExplosionFrames];
            InitializingDyingAnimationSourceRectangles();
            explosionAnimFrame = MaxExplosionFrames - 1;

            alive = false;
            enemyState = EnemyState.Alive;
            enemyDirection = EnemyDirection.Up;

        } // End of EnemyRanged()
        /// <summary>
        /// Enemy Constructor,
        /// constructs at a random location,
        /// x value between 600 to 900,
        /// y value between 150 to 550
        /// </summary>
        /// <param name="Rng">Random number generator</param>
        public EnemyRanged(Random Rng)
        {
            int x = SpawnLocX;
            int y = Rng.Next(150, 550);
            respawnTimer = 0;
            inLineOfSight = false;
            position = new Vector2(x, y);
            boundaryBox = new Rectangle(x, y, Width, Height);
            centerPosition = new Vector2(
                boundaryBox.Center.X, boundaryBox.Center.Y
                );
            bullet = new Bullet(centerPosition, BulletSpeed);

            // constructing explosion animation
            explosionSource = new Rectangle[MaxExplosionFrames];
            InitializingDyingAnimationSourceRectangles();
            explosionAnimFrame = MaxExplosionFrames - 1;

            alive = false;
            enemyState = EnemyState.Alive;
            enemyDirection = EnemyDirection.Up;

        } // End of EnemyRanged(Random)

        #endregion

        #region PUBLIC METHODS

        /// <summary>
        /// Loads all external content
        /// </summary>
        /// <param name="assetManager"></param>
        /// <param name="assetName"></param>
        public void LoadContent(ContentManager assetManager, string assetName)
        {
            /* Loading external textures content */
            // basic alove texture
            textureAlive = assetManager.Load<Texture2D>(assetName + "enemyship_alive");
            // up facing alive texture
            textureAliveUp = assetManager.Load<Texture2D>(assetName + "enemyship_alive_up");
            // down facing texture
            textureAliveDown = assetManager.Load<Texture2D>(assetName + "enemyship_alive_down");
            // left facing texture
            textureAliveLeft = assetManager.Load<Texture2D>(assetName + "enemyship_alive_left");
            // right facing texture
            textureAliveRight = assetManager.Load<Texture2D>(assetName + "enemyship_alive_right");
            // sets current texture to alive
            textureDraw = textureAlive;
            // basic dead texture
            textureDead = assetManager.Load<Texture2D>(assetName + "enemyship_dead");
            // loading bullet texture
            bullet.LoadContent(assetManager, assetName + "enemyship");
            // dying animation texture sheet
            LoadDyingAnimation(assetManager, assetName + ExplosionAnimationFolder);

            /* Loading external sound content */
            // dying sound
            soundDie = assetManager.Load<SoundEffect>(assetName + "enemyship_sound_dead");
            // firing sound
            soundFire = assetManager.Load<SoundEffect>(assetName + "enemyship_sound_fire");

        } // End of LoadContent()
        /// <summary>
        /// Allows the enemy to run logic such as updating itself,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="player">Used for the enemy to track the player</param>
        public void Update(Player player)
        {
            bullet.Update(centerPosition);
            FollowPlayer(player);
            AliveTracker();

            switch (enemyState)
            {
                case EnemyState.NoPlayer:
                    MoveNowhere();
                    break;
                case EnemyState.Alive:
                    // when enemy is alive do stuff
                    Move();
                    break;
                case EnemyState.Dying:
                    // run enemy dying stuff
                    RunDyingAnimation();

                    break;
                case EnemyState.Dead:
                    // when enemy is dead start respawning him
                    ReSpawning();

                    break;
                default:
                    // nothing
                    break;
            } // End of switch()

            TextureUpdater();

        } // End of Update()
        /// <summary>
        /// The enemy will draw itself,
        /// using a boundary box as a container,
        /// with no colour filter
        /// </summary>
        public void Draw(SpriteBatch spriteBatch)
        {
            switch (enemyState)
            {
                case EnemyState.Alive:

                    spriteBatch.Draw(textureDraw, boundaryBox, Color.White);
                    bullet.Draw(spriteBatch);

                    break;
                case EnemyState.Dying:

                    // draws explosion at enemy location
                    spriteBatch.Draw(textureExplosionAnim, boundaryBox, explosionFrame, Color.White);
                    // keeps drawing bullet
                    bullet.Draw(spriteBatch);

                    break;
                case EnemyState.Dead:

                    // when enemy is dead stop drawing him
                    bullet.Draw(spriteBatch);

                    break;
                case EnemyState.NoPlayer:

                    spriteBatch.Draw(textureDraw, boundaryBox, Color.White);
                    bullet.Draw(spriteBatch);

                    break;
                default:
                    // nothing
                    break;
            } // End of switch()

        } // End of Draw()
        /// <summary>
        /// Called when enemy is hit by a player bullet he dies
        /// </summary>
        public void BulletHit()
        {
            Die();

        } // End of BulletHit()
        
        #endregion

        #region CLASS PROPERTIES

        /// <summary>
        /// Gets the enemy's hit box, to be used by the Game class,
        /// for collisions
        /// </summary>
        public Rectangle BoundaryBox
        {
            get { return boundaryBox; }

        } // End of BoundaryBox
        /// <summary>
        /// Gets the bullet of the enemy for collision checking
        /// </summary>
        public Bullet Bullet
        {
            get { return bullet; }
        } // End of Bullet
        /// <summary>
        /// Gets the alive bool,
        /// used to detect whether enemy is alive or not
        /// </summary>
        public bool Alive
        {
            get { return alive; }
        }

        #endregion

        #region PRIVATE METHODS

        /// <summary>
        /// Will kill itself,
        /// set its state to dead,
        /// to start respawning
        /// </summary>
        private void Die()
        {
            soundDie.Play();
            enemyState = EnemyState.Dying;

        } // End of SoundDie()
        /// <summary>
        /// Will run every update,
        /// while enemy is dead,
        /// with a timer counting up until RespawnTime is hit,
        /// also place enemy at -100, -100 while respawning
        /// </summary>
        private void ReSpawning()
        {
            Place(RespawnLocX, RespawnLocY);
            if (respawnTimer >= RespawnTime)
            {
                ReSpawn();
            }
            else
            {
                respawnTimer++;
            }

        } // End of ReSpawning()
        /// <summary>
        /// Will place the enemy at the specified location
        /// </summary>
        /// <param name="x">x coordinate</param>
        /// <param name="y">y coordinate</param>
        private void Place(int x, int y)
        {
            position = new Vector2(x, y);
            LocationUpdateNotBullet();

        } // End of Place()
        /// <summary>
        /// Will ReSpawn the enemy,
        /// at a random location
        /// </summary>
        private void ReSpawn()
        {
            int x = SpawnLocX;
            int y = Global.randNumGen.Next(50, 550);
            respawnTimer = 0;

            position = new Vector2(x, y);
            boundaryBox = new Rectangle(x, y, Width, Height);
            centerPosition = new Vector2((boundaryBox.Right - boundaryBox.Left) / 2, (boundaryBox.Bottom - boundaryBox.Top) / 2);
            bullet.Die();

            enemyState = EnemyState.Alive;

        } // End of ReSpawn()
        /// <summary>
        /// Will update the current texture,
        /// by what direction the enemy is in
        /// </summary>
        private void TextureUpdater()
        {
            switch (enemyState)
            {
                case EnemyState.NoPlayer:
                case EnemyState.Alive:

                    switch (enemyDirection)
                    {
                        case EnemyDirection.Up:
                            textureDraw = textureAliveUp;
                            break;
                        case EnemyDirection.Right:
                            textureDraw = textureAliveRight;
                            break;
                        case EnemyDirection.Down:
                            textureDraw = textureAliveDown;
                            break;
                        case EnemyDirection.Left:
                            textureDraw = textureAliveLeft;
                            break;
                        default:
                            break;

                    } // End of switch()

                    break;
                case EnemyState.Dead:

                    textureDraw = textureDead;
                    
                    break;
                case EnemyState.Dying:
                default:
                    // nothing
                    break;

            } // End of switch()

        } // End of TextureUpdater()
        /// <summary>
        /// Will set the enemy's direction based on the passed player
        /// </summary>
        /// <param name="player">player object passed</param>
        private void FollowPlayer(Player player)
        {
            if (player.Alive == true)
            {
                if (boundaryBox.Y < player.BoundaryBox.Y - boundaryBox.Height)
                {
                    enemyDirection = EnemyDirection.Down;
                }
                else if (boundaryBox.Y > player.BoundaryBox.Y + player.BoundaryBox.Height)
                {
                    enemyDirection = EnemyDirection.Up;
                }
                if (boundaryBox.Y > player.BoundaryBox.Y - player.BoundaryBox.Height &&
                    boundaryBox.Y < player.BoundaryBox.Y + player.BoundaryBox.Height)
                {
                    if (boundaryBox.X > player.BoundaryBox.X + player.BoundaryBox.Width)
                    {
                        enemyDirection = EnemyDirection.Left;
                    }
                    if (boundaryBox.X < player.BoundaryBox.X - player.BoundaryBox.Width)
                    {
                        enemyDirection = EnemyDirection.Right;
                    }
                    inLineOfSight = true;
                }
                else
                {
                    inLineOfSight = false;
                }
            }
            else
            {
                enemyState = EnemyState.NoPlayer;
            }


        } // End of FollowPlayer()
        /// <summary>
        /// Will move the enemy based on,
        /// his direction
        /// </summary>
        private void Move()
        {
            switch (enemyDirection)
            {
                case EnemyDirection.Up:
                    MoveUp();
                    break;
                case EnemyDirection.Right:
                    break;
                case EnemyDirection.Down:
                    MoveDown();
                    break;
                case EnemyDirection.Left:
                    break;
                default:
                    MoveNowhere();
                    break;
            } // End of switch()
            Go(); // This will move the enemy
            FireInSight();
            LocationUpdate();

        } // End of Move()
        /// <summary>
        /// Will fire if player in line of sight
        /// </summary>
        private void FireInSight()
        {
            if (inLineOfSight == true)
            {
                Fire();
                if (bullet.Alive == false)
                {
                    soundFire.Play();
                }
            }

        } // End of FireInSight()
        /// <summary>
        /// Fires the bullet
        /// </summary>
        private void Fire()
        {
            switch (enemyDirection)
            {
                case EnemyDirection.Up:
                    bullet.Fire(Up);
                    break;
                case EnemyDirection.Right:
                    bullet.Fire(Right);
                    break;
                case EnemyDirection.Down:
                    bullet.Fire(Down);
                    break;
                case EnemyDirection.Left:
                    bullet.Fire(Left);
                    break;
                default:
                    break;
            }

        } // End of Fire()
        /// <summary>
        /// Will update the boundary box,
        /// to the enemy's position
        /// </summary>
        private void LocationUpdate()
        {
            boundaryBox.X = (int)position.X;
            boundaryBox.Y = (int)position.Y;
            centerPosition.X = boundaryBox.Center.X;
            centerPosition.Y = boundaryBox.Center.Y;

        } // End of LocationUpdate()
        /// <summary>
        /// Will update the boundary box,
        /// to the enemy's position
        /// </summary>
        private void LocationUpdateNotBullet()
        {
            boundaryBox.X = (int)position.X;
            boundaryBox.Y = (int)position.Y;

        } // End of LocationUpdateNotBullet()
        /// <summary>
        /// Will add the velocity vector,
        /// to the position vector,
        /// changing the enemies position
        /// </summary>
        private void Go()
        {
            position += velocity;

        } // End of Go()
        /// <summary>
        /// Will set players velocity vector to up
        /// </summary>
        private void MoveUp()
        {
            velocity = new Vector2(0, -MovementSpeed);

        } // End of MoveUp()
        /// <summary>
        /// Will set players velocity vector to down
        /// </summary>
        private void MoveDown()
        {
            velocity = new Vector2(0, MovementSpeed);

        } // End of MoveDown()
        /// <summary>
        /// Will set players velocity vector to right
        /// </summary>
        private void MoveRight()
        {
            velocity = new Vector2(MovementSpeed, 0);

        } // End of MoveRight()
        /// <summary>
        /// Will set players velocity vector to left
        /// </summary>
        private void MoveLeft()
        {
            velocity = new Vector2(-MovementSpeed, 0);

        } // End of MoveLeft()
        /// <summary>
        /// Sets the enemy's velocity vector to 0
        /// </summary>
        private void MoveNowhere()
        {
            velocity = new Vector2(0, 0);

        } // End of MoveNowhere()
        /// <summary>
        /// Will set the enemy to alive as soon as they spawn
        /// dependant on enemy's state
        /// </summary>
        private void AliveTracker()
        {
            switch (enemyState)
            {
                case EnemyState.NoPlayer:
                case EnemyState.Alive:
                    alive = true;
                    break;
                case EnemyState.Dying:
                case EnemyState.Dead:
                    alive = false;
                    break;
                default:
                    // nothing
                    break;
            }

        } // End of AliveTracker()
        /// <summary>
        /// Will load the dying explosion animation,
        /// using a sprite sheet
        /// </summary>
        /// <param name="assetManager"></param>
        /// <param name="assetName"></param>
        private void LoadDyingAnimation(ContentManager assetManager, string assetName)
        {
            textureExplosionAnim = assetManager.Load<Texture2D>(assetName + ExplosionAnimationFile);

        } // End of LoadDyingAnimation()
        /// <summary>
        /// Will create a array of rectangles,
        /// which will have every frame of the explosion
        /// starting at 0 up to 37 (total of 38 frames)
        /// </summary>
        private void InitializingDyingAnimationSourceRectangles()
        {
            int index = 0;
            for (int y = 600; y >= 0; y -= 100)
            {
                for (int x = 500; x >= 0; x -= 100)
                {
                    if (!(x > 100 && y == 600))
                    {
                        explosionSource[index] = new Rectangle(x, y, ExplosionWidth, ExplosionHeight);
                        index++;
                    }
                }
            }

        } // End of InitializingDyingAnimation()
        /// <summary>
        /// Will run dying animation,
        /// by itterating through,
        /// the source rectangle array
        /// than setting the enemy state to dead
        /// </summary>
        private void RunDyingAnimation()
        {
            if (explosionAnimFrame >= 0)
            {
                explosionFrame = explosionSource[explosionAnimFrame];
                explosionAnimFrame--;
            }
            else
            {
                explosionAnimFrame = MaxExplosionFrames - 1;
                enemyState = EnemyState.Dead;
            }

        } // End of RunDyingAnimation()

        #endregion

    } // End of class

} // End of namespace