﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description:
 * To give certain variables/constants
 * namespace level access
 */

namespace JointProject1
{
    class Global
    {
        #region VARIABLES

        public static int screenRightSide = 1000;
        public static int screenBottomSide = 600;
        public static int screenLeftSide = 0;
        public static int screenTopSide = 0;

        public static Random randNumGen;

        public enum PlayerState
        {
            Alive, Dead, Hit, Moving, Still
        }

        // Keyboard input
        // this is necessary to be public since keyboard input is required,
        // over multiple classes accross multiple instances
        public static KeyboardState previousKeyboardInput;
        public static KeyboardState currentKeyboardInput;

        #endregion

        #region CONSTANTS

        public const int PlayerWidth = 70;
        public const int PlayerHeight = 70;

        #endregion

    } // End of class

} // End of namespace
