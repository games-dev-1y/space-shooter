﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description:
 * Will be responsible for button,
 * updating and drawing of them
 */

namespace JointProject1
{
    class Button
    {
        #region VARIABLES

        // Starting position of the button (top left)
        Vector2 position;

        // box containing button texture
        Rectangle boundaryBox;

        // texture that will be used in the draw statement
        Texture2D textureCurrent;

        // texture for each button state
        Texture2D textureActive;
        Texture2D textureHover;
        Texture2D texturePress;
        
        // sound to play when the button is clicked
        SoundEffect buttonClickSound;

        // Will track mouse position
        Point mousePosition;
        MouseState currentMouseState, lastMouseState;

        // Represents button states
        enum ButtonState
        {
            Active, Hover, Pressed
        }
        ButtonState buttonState;

        // will track if button is pressed
        // to be used by game class as,
        // a buttons properties
        bool pressed;

        #endregion

        #region CONSTANTS

        // button dimensions
        const int Width = 300;
        const int Height = 150;

        // example
        const int Example = 0;

        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Default Constructor
        /// </summary>
        public Button()
        {
            position = new Vector2();
            boundaryBox = new Rectangle();
            buttonState = ButtonState.Active;
            currentMouseState = new MouseState();
            lastMouseState = currentMouseState;
            mousePosition = new Point(currentMouseState.X, currentMouseState.Y);
            pressed = false;

        } // End of Button()

        /// <summary>
        /// Constructor:
        /// Will construct button at specified vector
        /// </summary>
        /// <param name="location">Specific vector location</param>
        public Button(Vector2 location)
        {
            position = location;
            boundaryBox = new Rectangle((int)(location.X), (int)(location.Y), Width, Height);
            buttonState = ButtonState.Active;
            currentMouseState = new MouseState();
            lastMouseState = currentMouseState;
            mousePosition = new Point(currentMouseState.X, currentMouseState.Y);
            pressed = false;

        } // End of Button(Vector2)

        #endregion

        #region PUBLIC METHODS

        /// <summary>
        /// will load all the textures,
        /// for the various button states
        /// </summary>
        /// <param name="assetManager"></param>
        /// <param name="assetName"></param>
        public void LoadContent(ContentManager assetManager, string assetName)
        {
            textureActive = assetManager.Load<Texture2D>(assetName);
            textureHover = assetManager.Load<Texture2D>(assetName + "_hover");
            texturePress = assetManager.Load<Texture2D>(assetName + "_press");
            textureCurrent = textureActive;

            buttonClickSound = assetManager.Load<SoundEffect>(assetName + "_sound_click");

        } // End of LoadContent()
        /// <summary>
        /// Will handle all update logic,
        /// namely updating the button texture,
        /// based on mouse position
        /// </summary>
        public void Update()
        {
            RefreshMouseLocation();

            switch (buttonState)
            {
                case ButtonState.Active:
                    Active();
                    break;
                case ButtonState.Hover:
                    Hovered();
                    break;
                case ButtonState.Pressed:
                    Pressed();
                    break;
                default:
                    break;

            } // End of switch()

        } // End of Update()
        /// <summary>
        /// The button will draw itself
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(textureCurrent, boundaryBox, Color.White);

        } // End of Draw()

        #endregion

        #region CLASS PROPERTIES

        /// <summary>
        /// Gets the boundary box,
        /// containing the button
        /// </summary>
        public Rectangle BoundaryBox
        {
            get
            {
                return boundaryBox;
            }

        } // End of ClassPropertyExample
        /// <summary>
        /// Gets the pressed boolean,
        /// detecting whether or not,
        /// the button is pressed
        /// </summary>
        public bool IsPressed
        {
            get
            {
                return pressed;
            }

        } // End of Pressed

        #endregion

        #region PRIVATE METHODS

        /// <summary>
        /// When the button is hovered on,
        /// set the texture to,
        /// the hovered texture and,
        /// the pressed boolean to false
        /// </summary>
        private void Hovered()
        {
            textureCurrent = textureHover;
            pressed = false;

        } // End of Hovered()
        /// <summary>
        /// When the button is pressed on,
        /// set the texture to,
        /// the pressed texture and,
        /// the pressed boolean to true
        /// </summary>
        private void Pressed()
        {
            textureCurrent = texturePress;
            pressed = true;

        } // End of Pressed()
        /// <summary>
        /// When the button is not pressed or hovered on,
        /// set the texture to,
        /// active texture
        /// </summary>
        private void Active()
        {
            textureCurrent = textureActive;
            pressed = false;

        } // End of Active()
        /// <summary>
        /// Will track mouse location,
        /// based off that,
        /// set the button state correspondingly
        /// </summary>
        private void RefreshMouseLocation()
        {
            lastMouseState = currentMouseState;
            currentMouseState = Mouse.GetState();
            mousePosition = new Point(currentMouseState.X, currentMouseState.Y);

            if (CheckMouseHover(mousePosition))
            {
                if (CheckMousePress(currentMouseState, lastMouseState))
                {
                    buttonState = ButtonState.Pressed;
                }
                else
                {
                    buttonState = ButtonState.Hover;
                }
            }
            else
            {
                buttonState = ButtonState.Active;
            }

        } // End of CheckMouseLocation()
        /// <summary>
        /// Checks it the mouse is within 
        /// boundary box of the button
        /// </summary>
        /// <param name="position">the mouses position</param>
        /// <returns>if yes than its true, else its false</returns>
        private bool CheckMouseHover(Point position)
        {
            bool hovered;

            if (
                position.X > boundaryBox.X &&
                position.X < boundaryBox.X + boundaryBox.Width &&
                position.Y > boundaryBox.Y &&
                position.Y < boundaryBox.Y + boundaryBox.Height
                )
            {
                hovered = true;
            }
            else
            {
                hovered = false;
            }

            return hovered;

        } // End of CheckMouseHover()
        /// <summary>
        /// Checks if the mouse has pressed,
        /// the button
        /// </summary>
        /// <param name="currentState">the current mouse state</param>
        /// <param name="lastState">the last mouse state</param>
        /// <returns>if yes than its true, else its false</returns>
        private bool CheckMousePress(MouseState currentState, MouseState lastState)
        {
            bool pressed;

            if (currentState.LeftButton == Microsoft.Xna.Framework.Input.ButtonState.Pressed &&
                lastMouseState.LeftButton == Microsoft.Xna.Framework.Input.ButtonState.Released)
	        {
                pressed = true;
	        }
            else
            {
                pressed = false;
            }

            return pressed;

        } // End of CheckMousePress()

        #endregion

    } // End of class

} // End of namespace