﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description:
 * This is the player class which
 * represents the player controlled sprite,
 * possesses its own update and draw methods.
 */

namespace JointProject1
{
    class Player
    {
        #region VARIABLES

        // Will be the projectile that the player fires
        Bullet[] bullets;

        // Will store and alter the position of the player
        Vector2 position;

        // Will store the direction and velocity the player is moving
        Vector2 velocity;

        // Will be used to store the limitations of the player,
        // to be checked against the other sprites for collision
        Rectangle boundaryBox;

        // Will be used to play the players explosion animation,
        // as a source rectangle array to be assigned to explosionFrame
        Rectangle[] explosionSource;

        // Will be used as the source rectangle to draw,
        // players explosion animation
        Rectangle explosionFrame;

        // Small integer to itterate through,
        // the explosion source rectangle array
        Int16 explosionAnimFrame;

        // To animate explosion
        Texture2D textureExplosionAnim;

        // Will be used to store the center location of the texture,
        // to be used in the Draw() method
        Vector2 centerPosition;

        // Will be used to store the front location of the player,
        // to be used by the Bullet class
        Vector2 firingPosition;

        // Vector used to get the center when added to the position
        Vector2 toCenterPosition;

        // Vector used to get the top center when added to the center position
        Vector2 toTopCenterPosition;

        // Will be used to store the point on which the bullet rotates on
        Vector2 bulletPointOfRotation;

        // Currect Texture
        Texture2D spriteTexture;

        // Texture variable for alive or dead
        Texture2D textureAlive;
        Texture2D textureDead;

        // To animate the engine
        Texture2D[] textureEngineAnim;

        /* Will be used to store player sounds */
        // player firing
        SoundEffect soundFire;
        // player engines
        SoundEffect unistancedSoundEngines;
        SoundEffectInstance soundEngines;
        // player dying
        SoundEffect soundDie;

        // Used by the game class to determine whether the player,
        // is alive or dead
        bool alive;

        // Represents the current engine animation frame
        Int16 animationFrame;
        
        // Represents the amount of lives left
        int lives;

        // Represents the players current score
        int score;

        // Direction enumeration
        enum PlayerState
        {
            Alive, Dead, Hit, Moving, Still
        }

        // Represents the direction the player is facing
        PlayerState playerState;

        // Represents the rotation in degrees the player is facing
        float rotationAngle;

        // Represents number of active bullets on screen
        int activeBullets;

        #endregion

        #region CONSTANTS

        // Will represent the speed of the player
        const int MovementSpeed = 4; // per pixel
        const float RotationSpeed = 3.0f; // in degrees, per pixel

        // Will represent the dimensions of the player
        const int Width = Global.PlayerWidth;
        const int Height = Global.PlayerHeight;

        // Will represent the dimensions of the player explosion
        const int ExplosionWidth = 100;
        const int ExplosionHeight = 100;

        // Will represent players starting lives
        const int StartLives = 1;

        // Will represent maximum number of bullets on screen
        const int MaxBullets = 100;

        // Will represent max engine animation frames
        const int MaxAnimationFrames = 8;

        // Will represent max player explosion animation
        const int MaxExplosionFrames = 38;

        /* Player file paths */
        // Player alive texture
        const string TextureAliveFile = "playership_alive";
        // Player dead texture
        const string TextureDeadFile = "playership_dead";
        // player bullet file path
        const string BulletFilePath = "playership";
        // player engine animation folder
        const string EngineAnimationFolder = "animation/engine/";
        // player explosion animation folder
        const string ExplosionAnimationFolder = "animation/explosion/";
        // player explosion animation spritesheet
        const string ExplosionAnimationFile = "explosionsheet";
        // player sound firing file
        const string FiringSoundFile = "playership_sound_fire";
        // player sound dying file
        const string DyingSoundFile = "playership_sound_dead";
        // player sound engine file
        const string EngineSoundFile = "playership_sound_engines";

        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Default constructor,
        /// using the coordinate (100, 200),
        /// as a starting point.
        /// </summary>
        public Player()
        {
            int x = 100;
            int y = 200;

            // constructing vectors and boundary box
            position = new Vector2(x, y);
            boundaryBox = new Rectangle(x, y, Width, Height);
            centerPosition = new Vector2(boundaryBox.Right - (boundaryBox.Width / 2), boundaryBox.Bottom - (boundaryBox.Height / 2));
            firingPosition = new Vector2(boundaryBox.Right - (boundaryBox.Width / 2), boundaryBox.Top);
            
            // constructing bullets
            bullets = new Bullet[MaxBullets];
            activeBullets = 0;
            for (int i = 0; i < MaxBullets; i++)
            {
                bullets[i] = new Bullet();
            }
            x = 0; y = 600;
            
            // constructing explosion animation
            explosionSource = new Rectangle[MaxExplosionFrames];
            InitializingDyingAnimationSourceRectangles();
            explosionAnimFrame = MaxExplosionFrames - 1;

            // constructing engine animation
            textureEngineAnim = new Texture2D[MaxAnimationFrames];
            animationFrame = 0;

            // initializing variables
            playerState = PlayerState.Alive;
            lives = StartLives;
            rotationAngle = 0f;
            score = 0;

        } // End of Player()
        
        /// <summary>
        /// Player constructor,
        /// at a specific location vector
        /// </summary>
        /// <param name="location"></param>
        public Player(Vector2 location)
        {
            // constructing vectors
            position = location;
            boundaryBox = new Rectangle((int)location.X, (int)location.Y, Width, Height);
            toCenterPosition = new Vector2(Width/2, Height/2);
            toTopCenterPosition = new Vector2(0, -(Height/2));
            centerPosition = position + toCenterPosition;
            firingPosition = centerPosition + toTopCenterPosition;
            bulletPointOfRotation = centerPosition;
            
            // constructing bullet
            bullets = new Bullet[MaxBullets];
            activeBullets = 0;
            for (int i = 0; i < MaxBullets; i++)
            {
                bullets[i] = new Bullet();
            }
            location.X = 0; location.Y = 600;
            
            // constructing explosion animation
            explosionSource = new Rectangle[MaxExplosionFrames];
            InitializingDyingAnimationSourceRectangles();
            explosionAnimFrame = MaxExplosionFrames - 1;
            
            // constructing engine animation
            textureEngineAnim = new Texture2D[MaxAnimationFrames];
            animationFrame = 0;
            
            // initializing variables
            playerState = PlayerState.Alive;
            lives = StartLives;
            rotationAngle = 0f;
            score = 0;
            

        } // End of Player(location)

        #endregion

        #region PUBLIC METHODS

        /// <summary>
        /// Load all external content (audio and images)
        /// </summary>
        /// <param name="assetManager"></param>
        /// <param name="assetName"></param>
        public void LoadContent(ContentManager assetManager, string assetName)
        {
            /* loading external textures */
            // alive texture
            textureAlive = assetManager.Load<Texture2D>(assetName + TextureAliveFile);
            // dead texture
            textureDead = assetManager.Load<Texture2D>(assetName + TextureDeadFile);
            // engine animation textures
            LoadEngineAnimation(assetManager, assetName + EngineAnimationFolder);
            // dying animation texture sheet
            LoadDyingAnimation(assetManager, assetName + ExplosionAnimationFolder);
            // sets current texture to alive
            spriteTexture = textureAlive;

            /* loading external sounds */
            // loading firing sound
            soundFire = assetManager.Load<SoundEffect>(assetName + FiringSoundFile);
            // loading dying sound
            soundDie = assetManager.Load<SoundEffect>(assetName + DyingSoundFile);
            // creating instance of engine sound
            unistancedSoundEngines = assetManager.Load<SoundEffect>(assetName + EngineSoundFile);
            soundEngines = unistancedSoundEngines.CreateInstance();
            soundEngines.IsLooped = true;

            // loading bullet texture
            for (int i = 0; i < MaxBullets; i++)
            {
                bullets[i].LoadContent(assetManager, assetName + BulletFilePath);
            }

            // acquiring center position for rotations
            centerPosition = new Vector2((float)(spriteTexture.Width / 2), (float)(spriteTexture.Height / 2));

        } // End of LoadContent()
        /// <summary>
        /// will update class variables
        /// </summary>
        public void Update()
        {
            for (int i = 0; i < MaxBullets; i++)
            {
                bullets[i].Update(position);
            }
            TextureUpdate();
            AliveTracker();

            switch (playerState)
            {
                case PlayerState.Moving:
                case PlayerState.Still:
                case PlayerState.Alive:

                    Input();
                    BorderCollisions();

                    break;
                case PlayerState.Dead:

                    // 
                    StopEngineSound();
                    RunDyingAnimation();

                    break;
                case PlayerState.Hit:

                    //

                    break;
                default:
                    // nothing
                    break;
            }

        } // End of Update()
        /// <summary>
        /// // Player will draw itself
        /// </summary>
        public void Draw(SpriteBatch spriteBatch)
        {
            if (alive)
            {
                spriteBatch.Draw(spriteTexture, boundaryBox, null, Color.White, AngleToRadians(rotationAngle),
                    centerPosition, SpriteEffects.None, 0.0f);

                for (int i = 0; i < activeBullets; i++)
                {
                    bullets[i].Draw(spriteBatch);
                }
            }
            else
            {
                spriteBatch.Draw(spriteTexture, boundaryBox, null, Color.White, AngleToRadians(rotationAngle),
                    centerPosition, SpriteEffects.None, 0.0f);
                spriteBatch.Draw(textureExplosionAnim, boundaryBox, explosionFrame, Color.White);
            }

        } // End of Draw()
        /// <summary>
        /// When player hit,
        /// by a melee enemy,
        /// loses a life unless lives = 0,
        /// if so than player dies
        /// </summary>
        /// <param name="enemy">enemy of melee type</param>
        public void GotHitBy(EnemyMelee enemy)
        {
            lives--;
            if (lives < 0)
            {
                Die();
                lives = 0;
            }

        } // End of Hit(EnemyMelee)
        /// <summary>
        /// When player hit,
        /// loses a life unless lives = 0,
        /// if so than player dies
        /// </summary>
        public void Hit()
        {
            lives--;
            if (lives < 0)
            {
                Die();
                lives = 0;
            }

        } // End of Hit(EnemyRanged)
        /// <summary>
        /// Kills the player,
        /// setting the alive bool to false
        /// </summary>
        public void Die()
        {
            alive = false;
            playerState = PlayerState.Dead;
            soundDie.Play();

        } // End of Die()
        /// <summary>
        /// Will compute player collisions
        /// </summary>
        public void BorderCollisions()
        {
            CollidedWithScreenBorder();

        } // End of ComputeCollisions()
        /// <summary>
        /// Will stop engine sound from playing
        /// </summary>
        public void StopSounds()
        {
            StopEngineSound();

        } // End of StopSounds()

        #endregion

        #region CLASS PROPERTIES

        /// <summary>
        /// Will return the boundary box of the player,
        /// to be used in collisions, in the game class
        /// </summary>
        public Rectangle BoundaryBox
        {
            get
            {
                return boundaryBox;
            }

        } // End of BoundaryBox

        /// <summary>
        /// Gets the bullets object array that the player is using
        /// </summary>
        public Bullet[] Bullets
        {
            get { return bullets; }

        } // End of Bullet

        /// <summary>
        /// Gets the alive bool of the player
        /// </summary>
        public bool Alive
        {
            get
            {
                return alive;
            }

        } // End of Alive

        /// <summary>
        /// Gets the currecnt lives of the player
        /// </summary>
        public int Lives
        {
            get
            {
                return lives;
            }
        } // End of Lives

        /// <summary>
        /// Gets or sets the players score
        /// </summary>
        public int Score
        {
            get
            {
                return score;
            }
            set
            {
                score = value;
            }

        } // End of Score

        #endregion

        #region PRIVATE METHODS

        /// <summary>
        /// Detects whether or not the player has left the,
        /// confines of a border, offseting the player to remain,
        /// within the border
        /// </summary>
        private void CollidedWithScreenBorder()
        {
            Rectangle border = new Rectangle(
                Global.screenLeftSide,
                Global.screenTopSide,
                Global.screenRightSide,
                Global.screenBottomSide);

            if ( position.Y > border.Bottom - (boundaryBox.Height / 2) )
            {
                position -= velocity;
                LocationUpdate();
                if (playerState == PlayerState.Still)
                {
                    position.Y = border.Bottom - (boundaryBox.Height / 2);
                }
            }
            if ( position.Y < border.Top + (boundaryBox.Height / 2) )
            {

                position -= velocity;
                LocationUpdate();
                if (playerState == PlayerState.Still)
                {
                    position.Y = border.Top + (boundaryBox.Height / 2);
                }
                
            }
            if (position.X > border.Right - (boundaryBox.Width / 2) )
            {
                position -= velocity;
                LocationUpdate();
                if (playerState == PlayerState.Still)
                {
                    position.X = border.Right - (boundaryBox.Width / 2);
                }
            }
            if (position.X < border.Left + (boundaryBox.Width / 2))
            {
                position -= velocity;
                LocationUpdate();
                if (playerState == PlayerState.Still)
                {
                    position.X = border.Left + (boundaryBox.Width / 2);
                }
            }

        } // End of CollidedWithScreenBorder()
        /// <summary>
        /// Wil handle all input for handling the player class
        /// </summary>
        private void Input()
        {
            // Tracks keyboard presses
            Global.previousKeyboardInput = Global.currentKeyboardInput;
            Global.currentKeyboardInput = Keyboard.GetState();
            // Movement related keys
            Move(Global.currentKeyboardInput);
            // Firing related keys
            Fire(Global.currentKeyboardInput, Global.previousKeyboardInput);


        } // End of Input()
        /// <summary>
        /// Will handle player movement based on keyboard input
        /// </summary>
        /// <param name="keyboardInput">key pressed</param>
        private void Move(KeyboardState keyboardInput)
        {
            
            MoveNowhere();
            if (keyboardInput.IsKeyDown(Keys.Up))
            {
                MoveForwards();
                playerState = PlayerState.Moving;
                
            }
            else if (keyboardInput.IsKeyDown(Keys.Down))
            {
                MoveBackwards();
                playerState = PlayerState.Moving;
            }
            if (keyboardInput.IsKeyDown(Keys.Right))
            {
                RotateClockwise();
            }
            else if (keyboardInput.IsKeyDown(Keys.Left))
            {
                RotateAntiClockwise();
            }
            position += velocity;

            if ( !(ArrowKeyPressed(keyboardInput)) )
            {
                StopEngineSound();
                StopEngineAnimation();
            }

            LocationUpdate();

        } // End of Move()
        /// <summary>
        /// Will update the location based off the position vector
        /// </summary>
        private void LocationUpdate()
        {
            boundaryBox.X = (int)position.X;
            boundaryBox.Y = (int)position.Y;
            if (playerState == PlayerState.Alive)
            {
                centerPosition = new Vector2((float)(spriteTexture.Width / 2), (float)(spriteTexture.Height / 2));
            }
            //firingPosition = new Vector2(position.X - bullet.Width, position.Y - bullet.Height);

        } // End of UpdateLocation()
        /// <summary>
        /// Will handle all inputs for firing
        /// </summary>
        /// <param name="previousKeyboardState">key pressed 1 frame ago</param>
        /// <param name="currentKeyboardState">key pressed</param>
        private void Fire(KeyboardState currentKeyboardState, KeyboardState previousKeyboardState)
        {
            if (
                currentKeyboardState.IsKeyDown(Keys.Space) &&
                previousKeyboardState.IsKeyUp(Keys.Space)
                )
            {
                FireBullet();
                soundFire.Play();
            }

        } // End of Firing()
        /// <summary>
        /// Player fires a bullet
        /// </summary>
        private void FireBullet()
        {
            bullets[activeBullets].Fire(rotationAngle);
            activeBullets++;
            if (activeBullets >= MaxBullets)
            {
                activeBullets = 0;
            }

        } // End of Fire()
        /// <summary>
        /// Will move forwards, the player based on the direction its faced
        /// by applying the rotation angle to its velocity vector
        /// </summary>
        private void MoveForwards()
        {
            velocity = new Vector2(0, MovementSpeed);
            velocity = RotateVector(velocity, rotationAngle);
            RunEngineAnimation();
            StartEngineSound();

        } // End of MoveForward()
        /// <summary>
        /// Will create a array of rectangles,
        /// which will have every frame of the explosion
        /// starting at 0 up to 37 (total of 38 frames)
        /// </summary>
        private void InitializingDyingAnimationSourceRectangles()
        {
            int index = 0;
            for (int y = 600; y >= 0; y -= 100)
            {
                for (int x = 500; x >= 0; x -= 100)
                {
                    if (!(x > 100 && y == 600))
                    {
                        explosionSource[index] = new Rectangle(x, y, ExplosionWidth, ExplosionHeight);
                        index++;
                    }
                }
            }

        } // End of InitializingDyingAnimation()
        /// <summary>
        /// Will load the dying explosion animation,
        /// using a sprite sheet
        /// </summary>
        /// <param name="assetManager"></param>
        /// <param name="assetName"></param>
        private void LoadDyingAnimation(ContentManager assetManager, string assetName)
        {
            textureExplosionAnim = assetManager.Load<Texture2D>(assetName + ExplosionAnimationFile);

        } // End of LoadDyingAnimation()
        /// <summary>
        /// Will run dying animation,
        /// by itterating through,
        /// the source rectangle array
        /// </summary>
        private void RunDyingAnimation()
        {
            if (explosionAnimFrame >= 0)
            {
                explosionFrame = explosionSource[explosionAnimFrame];
                explosionAnimFrame--;
            }

        } // End of RunDyingAnimation()
        /// <summary>
        /// Loads all textures into the textures array
        /// </summary>
        /// <param name="assetManager"></param>
        /// <param name="assetName"></param>
        private void LoadEngineAnimation(ContentManager assetManager, string assetName)
        {
            while (animationFrame < 8)
            {
                textureEngineAnim[animationFrame] = assetManager.Load<Texture2D>(assetName + animationFrame.ToString());
                animationFrame++;
            }
            animationFrame = 0;

        } // End of LoadEngineAnimation()
        /// <summary>
        /// Runs the engine animation
        /// </summary>
        private void RunEngineAnimation()
        {
            spriteTexture = textureEngineAnim[animationFrame];

            animationFrame++;
            if (animationFrame > 7)
            {
                animationFrame = 0;
            }

        } // End of EngineAnimation()
        /// <summary>
        /// Stops the engine animation
        /// </summary>
        private void StopEngineAnimation()
        {
            spriteTexture = textureAlive;

        } // End of StopEngineAnimation()
        /// <summary>
        /// Will move backwards, the player based on the direction its faced
        /// by applying the rotation angle to its velocity vector
        /// </summary>
        private void MoveBackwards()
        {
            velocity = new Vector2(0, -MovementSpeed);
            velocity = RotateVector(velocity, rotationAngle);
            StartEngineSound();

        } // End of MoveBackwards()
        /// <summary>
        /// Will keep the player still
        /// </summary>
        private void MoveNowhere()
        {
            velocity = new Vector2(0, 0);
            playerState = PlayerState.Still;

        } // End of MoveNowhere()
        /// <summary>
        /// if engine sound is not playing,
        /// it will start playing the engine sound
        /// </summary>
        private void StartEngineSound()
        {
            if (soundEngines.State != SoundState.Playing)
            {
                soundEngines.Play();
            }

        } // End of StartEngineSound()
        /// <summary>
        /// if the engine sound is playing,
        /// it will stop the engine sound
        /// </summary>
        private void StopEngineSound()
        {
            if (soundEngines.State == SoundState.Playing)
            {
                soundEngines.Stop();
            }

        } // End of StopEngineSound()
        /// <summary>
        /// Will alter the update based on the state of the player
        /// alive or dead
        /// </summary>
        private void TextureUpdate()
        {
            if (alive)
            {
                spriteTexture = textureAlive;
            }
            else // Player is dead
            {
                spriteTexture = textureDead;
            }

        } // End of UpdateTexture()
        /// <summary>
        /// Rotates the sprite clockwise by incrementing
        /// the angle by the rotation speed
        /// </summary>
        private void RotateClockwise()
        {
            rotationAngle += RotationSpeed;
            RotateFiringPosition();

        } // End of RotateClockwise()
        /// <summary>
        /// Rotates the sprite anti-clockwise by
        /// decrementing the angle by the rotation speed
        /// </summary>
        private void RotateAntiClockwise()
        {
            rotationAngle -= RotationSpeed;
            RotateFiringPosition();

        } // End of RotateClockwise()
        /// <summary>
        /// Rotates the firing position vector
        /// </summary>
        private void RotateFiringPosition()
        {
            Vector2 positionCenter = position + toCenterPosition;
            firingPosition = RotateVectorAround(firingPosition, positionCenter, rotationAngle);

        } // End of RotateFiringPosition()
        /// <summary>
        /// Will update the alive bool,
        /// to be used as a class property and
        /// will set the spriteTexture to the dead texture
        /// </summary>
        private void AliveTracker()
        {
            if ( !(playerState == PlayerState.Dead) )
            {
                alive = true;
            }
            else
            {
                alive = false;
                spriteTexture = textureDead;
            }

        } // End of AliveTracker()
        /// <summary>
        /// Checks if any of the arrow keys,
        /// have been pressed
        /// </summary>
        /// <param name="keyboardInput">for keyboard input</param>
        /// <returns>if arrow key pressed than true, else false</returns>
        private bool ArrowKeyPressed(KeyboardState keyboardInput)
        {   
            bool keyPressed = false;

            if (
                keyboardInput.IsKeyDown(Keys.Up) ||
                keyboardInput.IsKeyDown(Keys.Down)
                )
            {
                keyPressed = true;
            }

            return keyPressed;

        } // End of KeyPressed()
        /// <summary>
        /// The vector to rotate will be rotated,
        /// by angle in degrees,
        /// around the vector center,
        /// by the following formula:
        /// X =
        /// (cosTheta * (vectorToRotate.X - vectorCenter.X) - sinTheta * (vectorToRotate.Y - vectorCenter.Y) + vectorCenter.X)
        /// Y =
        /// (sinTheta * (vectorToRotate.X - vectorCenter.X) + cosTheta * (vectorToRotate.Y - vectorCenter.Y) + vectorCenter.Y)
        /// </summary>
        /// <param name="vectorToRotate">Vector that gets rotated</param>
        /// <param name="vectorCenter">Vector center of the rotation</param>
        /// <param name="angleInDegrees">The rotation angle in degrees</param>
        /// <returns></returns>
        private Vector2 RotateVectorAround(
            Vector2 vectorToRotate,Vector2 vectorCenter, float angleInDegrees)
        {
            double angleInRadians = AngleToRadians(angleInDegrees);
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);

            return new Vector2(
                (float)
                (cosTheta * (vectorToRotate.X - vectorCenter.X) -
                sinTheta * (vectorToRotate.Y - vectorCenter.Y) + vectorCenter.X),
                (float)
                (sinTheta * (vectorToRotate.X - vectorCenter.X) -
                cosTheta * (vectorToRotate.Y - vectorCenter.Y) + vectorCenter.X)
            );

        } // End of RotateVectorAround()
        /// <summary>
        /// The vector to rotate will be rotated,
        /// by angle in degrees,
        /// around it own vector,
        /// by using the following formula:
        /// X =
        /// -(cos(radians) * velocity.X - sin(radians) * velocity.Y)
        /// Y = 
        /// (sin(radians) * velocity.X - cos(radians) * velocity.Y)
        /// </summary>
        /// <param name="vectorToRotate"></param>
        /// <param name="angleInDegrees"></param>
        /// <returns></returns>
        private Vector2 RotateVector(Vector2 vectorToRotate, float angleInDegrees)
        {
            double angleInRadians = AngleToRadians(angleInDegrees);
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);

            return new Vector2(
                (float)
                -(cosTheta * vectorToRotate.X - sinTheta * vectorToRotate.Y),
                (float)
                (sinTheta * vectorToRotate.X - cosTheta * vectorToRotate.Y)
                );

        } // End of RotateVector()
        /// <summary>
        /// Will convert degrees into radians
        /// </summary>
        /// <param name="inDegrees"> angle in degrees</param>
        private float AngleToRadians(float inDegrees)
        {
            const double AngleToDegrees = Math.PI / 180.0;
            return (float)(inDegrees * AngleToDegrees);

        } // End of AngleToRadians()
        
        #endregion

    } // End of class

} // End of namespace