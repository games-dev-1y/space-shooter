﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description:
 *  - Will handle game over screen text
 */

namespace JointProject1
{
    class GameOver
    {
        #region VARIABLES

        /* for loading background texture */
        Texture2D background;

        // container for texture
        Rectangle gameScreen;

        // for writing on the screen
        SpriteFont fontPen;

        // start of writing position
        Vector2 textPosition;

        // the message container
        string screenText;

        // time tracker
        int timer;

        // tracks letter types
        int lettersTyped;

        // for the game class to see when game over is finished
        bool finished;

        #endregion

        #region CONSTANTS

        /* asset path strings */
        // game over background
        const string GameOverBackground = "gameover_background";
        // game over font pen
        const string FontPen = "SpriteFont1";

        // time it takes to type on letter
        const int TimeToType = 50;

        // total amount of time, to be multiplied by time to type,
        // to get the amount of time spent typing on this screen
        const int TotalTime = 11;

        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Default constructor,
        /// </summary>
        public GameOver()
        {
            background = null;
            fontPen = null;
            screenText = "";
            timer = 0;
            lettersTyped = 0;
            finished = false;
            gameScreen = new Rectangle(0, 0, Global.screenRightSide, Global.screenBottomSide);
            textPosition = new Vector2((float)(Global.screenRightSide / 2) - 50, (float)(Global.screenBottomSide / 2));

        } // End of GameOver()

        #endregion

        #region PUBLIC METHODS

        /// <summary>
        /// Will load all external content for the game over screen
        /// </summary>
        /// <param name="assetManager"></param>
        /// <param name="assetName"></param>
        public void LoadContent(ContentManager assetManager, string assetName)
        {
            background = assetManager.Load<Texture2D>(assetName + GameOverBackground);
            fontPen = assetManager.Load<SpriteFont>(assetName + FontPen);

        } // End of LoadContent()
        /// <summary>
        /// Will update the screen text until,
        /// all text has been typed,
        /// than sets the finished bool to true
        /// </summary>
        public void Update()
        {
            Global.previousKeyboardInput = Global.currentKeyboardInput;
            Global.currentKeyboardInput = Keyboard.GetState();

            // will skip game over screen if a key is pressed,
            // and was no key was pressed in the previous key
            if (
                Global.currentKeyboardInput.GetPressedKeys().Length > 0 &&
                !(Global.previousKeyboardInput.GetPressedKeys().Length > 0)
                )
            {
                finished = true;
            }
            RefreshMessage();

        } // End of Update()
        /// <summary>
        /// draws the background and text to screen
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(fontPen, screenText, textPosition, Color.White);

        } // End of Draw()

        #endregion

        #region CLASS PROPERTIES

        /// <summary>
        /// Gets the boolean finished,
        /// used by game class to see if screen if finished
        /// </summary>
        public bool Finished
        {
            get { return finished; }

        } // End of Finished

        #endregion

        #region PRIVATE METHODS

        /// <summary>
        /// Will update the message every call
        /// </summary>
        private void RefreshMessage()
        {
            if (lettersTyped == TotalTime)
            {
                finished = true;
            }
            else
            {
                if (timer >= TimeToType)
                {
                    IterateLetterToType();
                    lettersTyped++;
                    timer = 0;
                }
                timer++;
            }

        } // End of RefreshMessage()
        /// <summary>
        /// Will add the appropriate letter to the message
        /// </summary>
        private void IterateLetterToType()
        {
            switch (lettersTyped)
            {
                case 0:
                    screenText += "G";
                    break;
                case 1:
                    screenText += "A";
                    break;
                case 2:
                    screenText += "M";
                    break;
                case 3:
                    screenText += "E";
                    break;
                case 4:
                    screenText += " O";
                    break;
                case 5:
                    screenText += "V";
                    break;
                case 6:
                    screenText += "E";
                    break;
                case 7:
                    screenText += "R ";
                    break;
                case 8:
                    screenText += "!";
                    break;
                case 9:
                    screenText += "";
                    break;
                default:
                    break;
            } // End of switch()

        } // End of 

        #endregion

    } // End of class

} // End of namespace