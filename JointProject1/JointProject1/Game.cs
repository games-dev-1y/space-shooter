using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Name: Rafael Girao
 * Student ID: C00203250
 */
/* Joint Project 1 with Arrays:
 *  - Improve your game (from Joint Project 1)
 *  - > Add 2 arrays
 *  - - - > Added a array used to store the source rectangles for the explosion animation
 *  - - - > WIP...
 *  - > make all instance variables private (unless constants or intentional public data)
 *  - - - > Done.
 *  - > Implement recommended changes and make improvements
 *  - - - > WIP...
 */
/* Improvements:
 *  - Added Score to the screen, Woohoo!
 *  - Created explosion animation for player death:
 *  - > By creating a rectangle array,
 *  to be used as the source rectangle,
 *  for the explosion sprite sheet texture.
 *  - Allowed Game over screen to be skippable
 *  - Created a explosion animation for every enemy death:
 *  - > By creating a rectangle array,
 *  to be used as the source rectangle,
 *  for the explosion sprite sheet texture.
 *  - Removed Music and Sound classes,
 *  used Song class to play background and mainmenu songs
 *  allowing XNA to load .mp3 files, hence saving up,
 *  to 90 megabytes of space
 */
/* Fixes:
 *  - Player dying if he went to the top left corner of the screen.
 *  - Player being able to "kill" enemies before they spawn,
 *  triggering them to respawn,
 *  while also playing the hit sound.
 *  - Ranged enemy dying, but bullet dying aswell.
 */
/* Worked on:
 *  11th/01
 *      from:   09:00
 *      to:     11:00 (2 hours)
 *  12th/01
 *      from:   15:00
 *      to:     16:00 (1 hour)
 *  13th/01
 *      from:   09:00
 *      to:     11:00 (2 hours)
 *  14th/01
 *      from:   15:00
 *      to:     19:00 (4 hours)
 *  19th/01
 *      from:   09:00
 *      to:     10:00 (1 hour)
 *  20th/01
 *      from:   09:00
 *      to:     12:00 (3 hours)
 *  21st/01
 *      from:   20:00
 *      to:     24:00 (4 hours)
 *  22nd/01
 *      from:   09:00
 *      to:     13:00 (4 hours)
 *      from:   16:00
 *      to:     21:00 (5 hours)
 *  23rd/01
 *      from:   14:00
 *      to:     21:00 (7 hours)
 *  24th/01
 *      from:   14:00
 *      to:     21:00 (7 hours)
 *  25th/01
 *      from:   09:00
 *      to:     11:00 (2 hours)
 *      from:   14:00
 *      to:     18:00 (4 hours)
 *  26th/01
 *      from:   11:00
 *      to:     12:00 (1 hour)
 *  27th/01
 *      from:   09:00
 *      to:     11:00 (2 hours)
 *  08th/02
 *      from:   09:00
 *      to:     11:00 (2 hours)
 *  15th/02
 *      from:   10:00
 *      to:     21:00 (11 hours)
 * 
 * Total time spent:
 *      56h:00min
 */
/* Known bugs:
 *  None
 */

namespace JointProject1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game : Microsoft.Xna.Framework.Game
    {
        #region VARIABLES

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // The Game Over screen
        GameOver gameOverScreen;

        // The Player
        Player thePlayer;

        // Enemy type easy (melee based)
        EnemyMelee[] theEnemyMelee = new EnemyMelee[EnemyMeleeAmount];

        // Enemy type hard (ranged based)
        EnemyRanged[] theEnemyRanged = new EnemyRanged[EnemyRangedAmount];

        // enemy current spawning
        double enemyMeleeSpawn;
        double enemyRangedSpawn;

        // Buttons
        Button btnStartGame;
        Button btnExitGame;

        // The screen border
        Rectangle screenBorder;

        // Vector for the center of the screen
        Vector2 screenCenter;

        /* Game Sounds */
        // main menu music
        Song mainMenuSong;
        // background music
        Song backgroundSong;

        /* Main Menu */
        // Loads the background for the main menu
        Texture2D mainMenuTexture;
        // Loads the background for the game
        Texture2D gameTexture;
        // will be the size of the game border
        Rectangle gameBorder;

        // will be used to draw the players score
        SpriteFont fontPen;

        // position of the text score
        Vector2 scorePosition;

        // position of the text player lives
        Vector2 livesPosition;

        // Detects what state the game is in
        enum GameState
        {
            Game, MainMenu, Over
        }

        GameState gameState;

        // will check if the game has started already
        bool gameRestarted = false;

        // game timer (per tick)
        double timer;

        #endregion

        #region CONSTANTS

        /* External file path names */
        // for the player
        const string ThePlayerSprite = "assets/player/";
        // for the enemy type melee
        const string TheEnemyMeleeSprite = "assets/enemy/melee/";
        // for the enemy type ranged
        const string TheEnemyRangedSprite = "assets/enemy/ranged/";
        // for loading sounds
        const string SoundFolder = "assets/sound/";
        // for loading mainmenu song
        const string MainMenuSong = "music_mainmenu";
        // for loading background song
        const string BackgroundSong = "music_background";
        // for loading main menu
        const string MainMenuFolder = "assets/mainmenu/";
        // for the background texture
        const string BackgroundTexture = "background";
        // for the start game button
        const string ButtonStartGame = "button/startgame/button_startgame";
        // for the exit game button
        const string ButtonExitGame = "button/exitgame/button_exitgame";
        // for loading game background
        const string GameBackground = "assets/game_background";
        // for loading game over background
        const string GameOverFolder = "assets/gameover/";
        // for loading the SpriteFont
        const string SpriteFont = "assets/SpriteFont1";

        // button placement
        const float StartGameButtonX = 350f;
        const float StartGameButtonY = 250f;
        const float ExitGameButtonX = 350f;
        const float ExitGameButtonY = 400f;

        // score positions
        const float ScorePositionX = 100;
        const float ScorePositionY = 20;

        // lives positions
        const float LivesPositionX = 650;
        const float LivesPositionY = 20;

        // enemy quantity
        const int EnemyMeleeAmount = 50;
        const int EnemyRangedAmount = 5;
        
        // level progression
        const double FirstLevel = 0.02;
        const double SecondLevel = 0.1;
        const uint SecondLevelStart = 500;
        const double ThirdLevel = 0.2;
        const uint ThirdLevelStart = 1000;
        const double FourthLevel = 0.5;
        const uint FourthLevelStart = 2000;
        const double FinalLevel = 1;
        const uint FinalLevelStart = 4000;

        // for drawing the score text
        const string ScoreText = "Score: ";

        // for drawing the lives text
        const string LivesText = "Lives: ";

        #endregion

        #region XNA OVERRIDE METHODS

        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            // Setting up game window
            graphics.PreferredBackBufferWidth = Global.screenRightSide;
            graphics.PreferredBackBufferHeight = Global.screenBottomSide;
            screenBorder = this.Window.ClientBounds;

        } // End of Game()
        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // Sets up the random number generator
            Global.randNumGen = new Random();

            // Sets up the screen center vector
            screenCenter.X = Global.screenRightSide/2;
            screenCenter.Y = Global.screenBottomSide/2;
            
            // Sets timer to 0
            timer = 0;

            // will set up the game border
            gameBorder = new Rectangle(
                0,
                0,
                graphics.GraphicsDevice.Viewport.Width,
                graphics.GraphicsDevice.Viewport.Height
                );

            // creates the player object in the center of the screen
            thePlayer = new Player(screenCenter);

            // the score position
            scorePosition = new Vector2(ScorePositionX, ScorePositionY);

            // the lives position
            livesPosition = new Vector2(LivesPositionX, LivesPositionY);

            // the score spritefont
            fontPen = null;

            // creates the enemies
            // of type melee
            for (int enemyIndex = 0; enemyIndex < EnemyMeleeAmount; enemyIndex++)
            {
                theEnemyMelee[enemyIndex] = new EnemyMelee(Global.randNumGen);
            }
            // of type ranged
            for (int enemyIndex = 0; enemyIndex < EnemyRangedAmount; enemyIndex++)
            {
                theEnemyRanged[enemyIndex] = new EnemyRanged(Global.randNumGen);
            }

            // creates the buttons
            // start game button
            Vector2 buttonLocation = new Vector2(StartGameButtonX, StartGameButtonY);
            btnStartGame = new Button(buttonLocation);
            // exit game button
            buttonLocation = new Vector2(ExitGameButtonX, ExitGameButtonY);
            btnExitGame = new Button(buttonLocation);

            gameOverScreen = new GameOver();

            // allows the mouse to be viewed on the game screen
            this.IsMouseVisible = true;

            if (gameState != GameState.MainMenu && gameRestarted == false)
            {
                // Sets the starting game state
                gameState = GameState.MainMenu;
                gameRestarted = true;
            }
            else
            {
                gameState = GameState.Game;
            }

            base.Initialize();

        } // End of Initialize()
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            // Will load all external content for the Player class
            thePlayer.LoadContent(this.Content, ThePlayerSprite);

            // Will load up our SpriteFont
            fontPen = Content.Load<SpriteFont>(SpriteFont);

            // Will load all external content for the EnemyMelee class
            for (int enemyIndex = 0; enemyIndex < EnemyMeleeAmount; enemyIndex++)
            {
                theEnemyMelee[enemyIndex].LoadContent(this.Content, TheEnemyMeleeSprite);
            }
            
            // Will load all external content for the EnemyRanged class
            for (int enemyIndex = 0; enemyIndex < EnemyRangedAmount; enemyIndex++)
            {
                theEnemyRanged[enemyIndex].LoadContent(this.Content, TheEnemyRangedSprite);
            }

            // Will load all external content for the Button class
            btnStartGame.LoadContent(this.Content, MainMenuFolder + ButtonStartGame);
            btnExitGame.LoadContent(this.Content, MainMenuFolder + ButtonExitGame);

            /* Will load all external sound content */
            // loading main menu song
            mainMenuSong = Content.Load<Song>(SoundFolder + MainMenuSong);
            // loading background song
            backgroundSong = Content.Load<Song>(SoundFolder + BackgroundSong);

            // Will load main menu background texture
            mainMenuTexture = Content.Load<Texture2D>(MainMenuFolder + BackgroundTexture);

            // Will load the game background texture
            gameTexture = Content.Load<Texture2D>(GameBackground);

            // Will load the game over stuff
            gameOverScreen.LoadContent(this.Content, GameOverFolder);

        } // End of LoadContent()
        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // TESTING PURPOSES ONLY: pressing escape immediately closes the program
            DebuggingInstructions();

            // Will proccess game depending on Game state
            switch (gameState)
            {
                case GameState.Game:

                    thePlayer.Update();
                    for (int enemyIndex = 0; enemyIndex < enemyMeleeSpawn; enemyIndex++)
                    {
                        theEnemyMelee[enemyIndex].Update(thePlayer);
                    }
                    for (int enemyIndex = 0; enemyIndex < enemyRangedSpawn; enemyIndex++)
                    {
                        theEnemyRanged[enemyIndex].Update(thePlayer);
                    }

                    ComputeObjectCollisions();

                    LevelProgression();

                    PlayBackgroundSound();
                    IsPlayerDead();

                    break; // End of GameState.Game

                case GameState.MainMenu:

                    PlayMainMenuSound();

                    btnStartGame.Update();
                    btnExitGame.Update();

                    CheckButtonPress();

                    break; // End of GameState.MainMenu

                case GameState.Over:

                    gameOverScreen.Update();
                    IsGameOverScreenFinished();
                    thePlayer.Update();

                    for (int enemyIndex = 0; enemyIndex < enemyMeleeSpawn; enemyIndex++)
                    {
                        theEnemyMelee[enemyIndex].Update(thePlayer);
                    }
                    for (int enemyIndex = 0; enemyIndex < enemyRangedSpawn; enemyIndex++)
                    {
                        theEnemyRanged[enemyIndex].Update(thePlayer);
                    }

                    break; // End of GameState.Over

                default:
                    // nothing
                    break;

            } // End of switch

            base.Update(gameTime);
        } // End of Update()
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin(); // Following commands will be sent to GPU

            // Will draw different things based on what state the game is in
            switch (gameState)
            {
                case GameState.Game:

                    // game background
                    spriteBatch.Draw(gameTexture, gameBorder, Color.White);

                    // player
                    thePlayer.Draw(this.spriteBatch);

                    // the players score
                    spriteBatch.DrawString(fontPen, ScoreText + thePlayer.Score, scorePosition, Color.White);
                    // the players lives
                    spriteBatch.DrawString(fontPen, LivesText + thePlayer.Lives, livesPosition, Color.White);

                    // enemies
                    for (int enemyIndex = 0; enemyIndex < enemyMeleeSpawn; enemyIndex++)
                    {
                        theEnemyMelee[enemyIndex].Draw(this.spriteBatch);
                    }
                    for (int enemyIndex = 0; enemyIndex < enemyRangedSpawn; enemyIndex++)
                    {
                        theEnemyRanged[enemyIndex].Draw(this.spriteBatch);
                    }

                    break; // End of GameState.Game
                case GameState.MainMenu:

                    // Draws background texture
                    spriteBatch.Draw(mainMenuTexture, gameBorder, Color.White);
                    // draws start game button
                    btnStartGame.Draw(this.spriteBatch);
                    // draws exit game button
                    btnExitGame.Draw(this.spriteBatch);

                    break; // End of GameState.MainMenu
                case GameState.Over:
                    // game background
                    spriteBatch.Draw(gameTexture, gameBorder, Color.White);

                    // the players score
                    spriteBatch.DrawString(fontPen, ScoreText + thePlayer.Score, scorePosition, Color.White);
                    // the players lives
                    spriteBatch.DrawString(fontPen, LivesText + thePlayer.Lives, livesPosition, Color.White);

                    // player
                    thePlayer.Draw(this.spriteBatch);

                    // enemies
                    for (int enemyIndex = 0; enemyIndex < enemyMeleeSpawn; enemyIndex++)
                    {
                        theEnemyMelee[enemyIndex].Draw(this.spriteBatch);
                    }
                    for (int enemyIndex = 0; enemyIndex < enemyRangedSpawn; enemyIndex++)
                    {
                        theEnemyRanged[enemyIndex].Draw(this.spriteBatch);
                    }
                    // Draws the game over screen
                    gameOverScreen.Draw(spriteBatch);

                    break; // End of GameState.Over
                default:
                    // nothing
                    break;

            } // End of switch
            

            spriteBatch.End(); // Stop sending commands to GPU

            base.Draw(gameTime);

        } // End of Draw()

        #endregion

        #region PRIVATE METHODS

        /// <summary>
        /// Alters enemy spawn everytime
        /// </summary>
        private void LevelProgression()
        {
            if (timer > FinalLevelStart)
            {
                enemyMeleeSpawn = Math.Truncate(EnemyMeleeAmount * FinalLevel);
                enemyRangedSpawn = Math.Truncate(EnemyRangedAmount * FinalLevel);
                timer = FinalLevelStart;
            }
            else if (timer > FourthLevelStart)
            {
                enemyMeleeSpawn = Math.Truncate(EnemyMeleeAmount * FourthLevel);
                enemyRangedSpawn = Math.Truncate(EnemyRangedAmount * FourthLevel);
            }
            else if (timer > ThirdLevelStart)
            {
                enemyMeleeSpawn = Math.Truncate(EnemyMeleeAmount * ThirdLevel);
                enemyRangedSpawn = Math.Truncate(EnemyRangedAmount * ThirdLevel);
            }
            else if (timer > SecondLevelStart)
            {
                enemyMeleeSpawn = Math.Truncate(EnemyMeleeAmount * SecondLevel);
                enemyRangedSpawn = Math.Truncate(EnemyRangedAmount * SecondLevel);
            }
            else
            {
                enemyMeleeSpawn = Math.Truncate(EnemyMeleeAmount * FirstLevel);
                enemyRangedSpawn = Math.Truncate(EnemyRangedAmount * FirstLevel);
            }
            timer++;
            thePlayer.Score += 10;

        } // End of LevelProgression()
        /// <summary>
        /// Checks if any of the buttons are pressed,
        /// 
        /// if start game button is pressed than,
        /// it set the game state to Game,
        /// 
        /// if exit game button is pressed than,
        /// it runs Game.Exit()
        /// </summary>
        private void CheckButtonPress()
        {
            if (btnStartGame.IsPressed)
            {
                gameState = GameState.Game;
                StopAllSounds();
                Initialize();
            }
            if (btnExitGame.IsPressed)
            {
                StopAllSounds();
                Exit();
            }

        } // End of CheckButtonPress()
        /// <summary>
        /// Will stop all sounds from playing
        /// </summary>
        private void StopAllSounds()
        {
            StopBackgroundSound();
            StopMainMenuSound();
            thePlayer.StopSounds();

        } // End of StopAllSounds()
        /// <summary>
        /// Will handle all object collisions with,
        /// both themselves and the screen edge
        /// </summary>
        private void ComputeObjectCollisions()
        {
            thePlayer.BorderCollisions();
            for (int i = 0; i < thePlayer.Bullets.Length; i++)
            {
                thePlayer.Bullets[i].BorderCollisions();
            }
            for (int enemyIndex = 0; enemyIndex < EnemyMeleeAmount; enemyIndex++)
            {
                ComputeCollisions(thePlayer, theEnemyMelee[enemyIndex]);;
            }
            for (int enemyIndex = 0; enemyIndex < EnemyRangedAmount; enemyIndex++)
            {
                theEnemyRanged[enemyIndex].Bullet.BorderCollisions();
                ComputeCollisions(thePlayer, theEnemyRanged[enemyIndex]);
            }            

        } // End of ComputeCollisions()
        /// <summary>
        /// Computes bullet collision between,
        /// a bullet and a enemy of type ranged
        /// </summary>
        /// <param name="bullet"></param>
        /// <param name="enemyRanged"></param>
        private void ComputeBulletCollision(Bullet bullet, EnemyRanged enemyRanged)
        {
            if (CheckRectangularCollision(bullet.BoundaryBox, enemyRanged.BoundaryBox))
            {
                if (enemyRanged.Alive == true)
                {
                    enemyRanged.BulletHit();
                    bullet.Die();
                    thePlayer.Score += 20;
                }
                
            }

        } // End of ComputeBulletCollision(Bullet, EnemyRanged)
        /// <summary>
        /// Computes bullet collision between,
        /// a bullet and a enemy of type melee
        /// </summary>
        /// <param name="bullet"></param>
        /// <param name="enemyMelee"></param>
        private void ComputeBulletCollision(Bullet bullet, EnemyMelee enemyMelee)
        {
            if (CheckRectangularCollision(bullet.BoundaryBox, enemyMelee.BoundaryBox))
            {
                if (enemyMelee.Alive == true)
                {
                    enemyMelee.BulletHit();
                    bullet.Die();
                    thePlayer.Score += 5;
                }
                
            }

        } // End of ComputeBulletCollision(Bullet, EnemyMelee)
        /// <summary>
        /// Computes bullet collision between,
        /// a bullet and a player
        /// </summary>
        /// <param name="bullet"></param>
        /// <param name="player"></param>
        private void ComputeBulletCollision(Bullet bullet, Player player)
        {
            if (CheckRectangularCollision(bullet.BoundaryBox, player.BoundaryBox))
            {
                thePlayer.Hit();
                bullet.Die();
            }

        } // End of ComputeBulletCollisions(Bullet, Player)
        /// <summary>
        /// Will compute collisions between,
        /// the player and,
        /// a enemy melee
        /// </summary>
        /// <param name="player">the player object</param>
        /// <param name="enemyMelee">the enemy of type melee</param>
        private void ComputeCollisions(Player player, EnemyMelee enemyMelee)
        {
            if (enemyMelee.Alive)
            {
                if (CheckRectangularCollision(player.BoundaryBox, enemyMelee.BoundaryBox))
                {
                    player.GotHitBy(enemyMelee);
                    enemyMelee.HitPlayer();
                }
                int bulletLength = player.Bullets.Length;
                for (int i = 0; i < bulletLength; i++)
                {
                    ComputeBulletCollision(player.Bullets[i], enemyMelee);
                }
            }
            
            

        } // End of ComputeCollisions(player,enemyMelee)
        /// <summary>
        /// Will compute collisions between,
        /// the player and,
        /// a enemy ranged
        /// </summary>
        /// <param name="player">to track player boundary box</param>
        /// <param name="enemyRanged">to track enemy of type ranged</param>
        private void ComputeCollisions(Player player, EnemyRanged enemyRanged)
        {
            if (enemyRanged.Alive)
            {
                if (CheckRectangularCollision(player.BoundaryBox, enemyRanged.BoundaryBox))
                {
                    player.Hit();
                }
                int bulletLength = player.Bullets.Length;
                for (int i = 0; i < bulletLength; i++)
                {
                    ComputeBulletCollision(player.Bullets[i], enemyRanged);
                }
                ComputeBulletCollision(enemyRanged.Bullet, player);
            }
            

        } // End of ComputeCollisions(player,enemyRanged)
        /// <summary>
        /// Checks if 2 rectangulars have intersected each other
        /// </summary>
        /// <param name="box1">first rectangle</param>
        /// <param name="box2">second rectangle</param>
        /// <returns>true if collided, false if not</returns>
        private bool CheckRectangularCollision(Rectangle box1, Rectangle box2)
        {
            bool collided;

            if ( box1.X < box2.X - box1.Width ||
                box1.X > box2.X + box2.Width ||
                box1.Y < box2.Y - box1.Height ||
                box1.Y > box2.Y + box2.Height)
	        {
		        collided = false;
	        }
            else
	        {
                collided = true;
	        }

            return collided;

        } // End of CheckRectangularCollision()
        /// <summary>
        /// Will start the background music instance
        /// </summary>
        private void PlayBackgroundSound()
        {
            if (MediaPlayer.State != MediaState.Playing)
            {
                MediaPlayer.Play(backgroundSong);
            }

        } // End of PlayBackgroundSound()
        /// <summary>
        /// Will stop the background music instance
        /// </summary>
        private void StopBackgroundSound()
        {
            if (MediaPlayer.State == MediaState.Playing)
            {
                MediaPlayer.Stop();
            }
            
        } // End of StopBackgroundSound()
        /// <summary>
        /// Starts playing the main menu sound,
        /// if it isnt already playing
        /// </summary>
        private void PlayMainMenuSound()
        {
            if (MediaPlayer.State != MediaState.Playing)
            {
                MediaPlayer.Play(mainMenuSong);
            }

        } // End of PlayMainMenuSound()
        /// <summary>
        /// if the main menu sound is playing,
        /// than it will be stopped
        /// </summary>
        private void StopMainMenuSound()
        {
            if (MediaPlayer.State == MediaState.Playing)
            {
                MediaPlayer.Stop();
            }

        } // End of StopMainMenuSound()
        /// <summary>
        /// Checks if player is alive,
        /// if so than this is skipped,
        /// if dead than game state changes
        /// </summary>
        private void IsPlayerDead()
        {
            if (thePlayer.Alive == true)
            {
                // Do nothing
            }
            else
            {
                // goes to game over screen
                StopAllSounds();
                gameState = GameState.Over;
            }

        } // End of IsPlayerDead()
        /// <summary>
        /// Checks if the game over screen is finished
        /// </summary>
        private void IsGameOverScreenFinished()
        {
            if (gameOverScreen.Finished == true)
            {
                StopAllSounds();
                gameState = GameState.MainMenu;
            }

        } // End of IsGameOverScreenFinished()
        /// <summary>
        /// TESTING PURPOSES ONLY:
        /// Escape key - exits program immediatly
        /// F1 to F4 - sets level progression
        /// </summary>
        private void DebuggingInstructions()
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState(PlayerIndex.One).IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }
            //if (Keyboard.GetState(PlayerIndex.One).IsKeyDown(Keys.F1))
            //{
            //    timer = 0;
            //}
            //if (Keyboard.GetState(PlayerIndex.One).IsKeyDown(Keys.F2))
            //{
            //    timer = SecondLevelStart;
            //}
            //if (Keyboard.GetState(PlayerIndex.One).IsKeyDown(Keys.F3))
            //{
            //    timer = ThirdLevelStart;
            //}
            //if (Keyboard.GetState(PlayerIndex.One).IsKeyDown(Keys.F4))
            //{
            //    timer = FourthLevelStart;
            //}

        } // End of DebuggingInstructions()

        #endregion

    } // End of class

} // End of namespace
